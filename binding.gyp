{
  "targets": [
    {
      "target_name": "vk-massive",
      "sources": [
        "vulkan_addon/cpp_source/OpenFBX/ofbx.cpp",
        "vulkan_addon/cpp_source/OpenFBX/miniz.c",
        "vulkan_addon/cpp_source/*.cpp",
      ],
      "include_dirs": [
        "<!(node -e \"require('nan')\")",
        "vulkan_addon/cpp_includes/glm",
        "vulkan_addon/cpp_includes/glfw",
        "vulkan_addon/cpp_includes/stb",
        "vulkan_addon/cpp_includes/vulkan",
        "vulkan_addon/cpp_includes/nlohmann",
        "vulkan_addon/cpp_includes/tinyobjloader",
        "vulkan_addon/cpp_includes/stl_reader",
        "vulkan_addon/cpp_includes",
      ],
      "msvs_settings": {
        "VCCLCompilerTool": {
          "AdditionalOptions": [
            "-std:c++17"
          ],
          "ExceptionHandling": 1
        }
      },
      'defines': [ 'NODE_API' ],
      "libraries": [
        "../vulkan_addon/cpp_libs/vulkan/vulkan-1",
        "../vulkan_addon/cpp_libs/vulkan/VkLayer_utils",
        "../vulkan_addon/cpp_libs/vulkan/shaderc_combined",
        "../vulkan_addon/cpp_libs/glfw/glfw3dll",
        "../vulkan_addon/cpp_libs/glfw/glfw3"
      ]
    }
  ]
}