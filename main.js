// Modules to control application life and create native browser window
const { app, BrowserWindow } = require('electron')
const ipc = require('electron').ipcMain;
var vkMassive = require('bindings')('vk-massive');
const path = require('path')

function waitForVkMassive() {
  vkMassive.is_ready();
}

function createWindow() {
  waitForVkMassive();

  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipc.handle('create-node', (event, args) => { return vkMassive.create_node(args) });
ipc.handle('get-nodes', (event, args) => { return vkMassive.get_nodes() });
ipc.handle('get-node', (event, args) => { return vkMassive.get_node(args) });
ipc.handle('update-node', (event, args) => { vkMassive.update_node(args) });
ipc.handle('unlink-node', (event, args) => { vkMassive.unlink_node(args) });
ipc.handle('link-node', (event, args) => { vkMassive.link_node(args) });
ipc.handle('start-render', (event, args) => { vkMassive.start_render(args) });

ipc.handle('get-camera', (event, args) => { return vkMassive.get_camera() });
ipc.handle('update-camera', (event, args) => { vkMassive.update_camera(args) });
ipc.handle('update-material', (event, args) => { vkMassive.update_node_material(args) });