// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
const ipc = require('electron').ipcRenderer

const expendedIconClass = "glyph-icon flaticon-expand-button"
const retractedIconClass = "glyph-icon flaticon-keyboard-right-arrow-button"
const childsClass = "node-childrens"
const nodeClass = "scene-node"
const coordinates = ['x', 'y', 'z']
const geometrySource = ['Cube', 'Pyramid', 'Sphere', 'Lindenmayer Tree', '3D Model']
const modelFileType = ['obj', 'stl', 'fbx']
const projectionType = ['Perspective', 'Orthogonal']
const materialOptionsClass = 'material-option'
const materialType = [
    {
        'title'     :   'Lambertian',
        'id'        :   'lambertian',
    },
    {
        'title'     :   'Diffuse Light',
        'id'        :   'diffLight',
    },
    {
        'title'     :   'Dielectric',
        'id'        :   'dielectric',
    },
    {
        'title'     :   'Metalic',
        'id'        :   'metalic',
    },
    {
        'title'     :   'CheckerBoard',
        'id'        :   'checkerboard',
    }
]


let scene = new Map()
let camera = {}
let selectedNode = []
let undoStack = []
let redoStack = []

function changeSelectedOption(selectId, optionValToSelect){
    var selectElement = document.getElementById(selectId);
    var selectOptions = selectElement.options;
    for (var opt, j = 0; opt = selectOptions[j]; j++) {
        if (opt.value == optionValToSelect) {
            selectElement.selectedIndex = j;
        }
    }
}

function childrenMatches(elem, selector) {
	return Array.prototype.filter.call(elem.children, function (child) {
		return child.matches(selector);
	});
};

function updateDisplayChilds(parent) {
    const displayTab = childrenMatches(parent.firstChild, '.glyph-icon')[0]
    const childsTab = childrenMatches(parent, "." + childsClass)[0]

    if(displayTab.className.includes(expendedIconClass)){
        childsTab.className = childsClass
    }

    if(displayTab.className.includes(retractedIconClass)){
        childsTab.className = childsClass + " hidden"
    }

}

function toggleDisplayChilds(elem) {
    if (elem.className == retractedIconClass) {
        elem.className = expendedIconClass
    }
    else {
        elem.className = retractedIconClass
    }
}

function changeSelectedTab(node, multipleSelect) {
    if (!multipleSelect) {
        for(let i = 0; i < selectedNode.length; i ++)
        {
            let className = selectedNode[i].className.replace(' selected', '')
            selectedNode[i].className = className
        }

        selectedNode = []
    }

    if(!selectedNode.includes(node) || !multipleSelect)
    {
        node.className += ' selected'
        selectedNode.push(node)
    }
}

function generateUndoRedoPackage(nodes) {
    let undoRedoInfo = []
    for(let i = 0; i < nodes.length; i ++)
    {
        const nodeId = parseInt(selectedNode[i].getAttribute("node-id"))
        const node = scene.get(nodeId)

        undoRedoInfo.push(node)
    }

    return undoRedoInfo
}

function saveNodes(nodes, actionType = "save") {
    switch(actionType)
    {
        case "save" :
            undoStack.push(generateUndoRedoPackage(nodes))
            redoStack = []
            break
        case "undo" :
            redoStack.push(generateUndoRedoPackage(nodes))
            break
        case "redo" :
            undoStack.push(generateUndoRedoPackage(nodes))
            break
    }

    nodes.forEach(node => {
        const dataPacket = JSON.stringify(node)

        ipc.invoke('update-node', dataPacket)
        ipc.invoke('get-node', node.id).then((data) => {
            let returnNode = JSON.parse(data)
            scene.set(node.id, returnNode);

            let tabs = document.getElementsByClassName('node-'+node.id)
            
            for(let tab of tabs)
            {
                let name = childrenMatches(childrenMatches(tab, '.node-info')[0], '.node-name')[0].firstChild
                name.innerText = node.name
            }
   
            updateEditForm()
            refreshAvailableNodes()
            updateEditForm()
        })
    });
}

function undo() {
    if(undoStack.length > 0)
    {
        const nodes = undoStack.pop()
        saveNodes(nodes, "undo")
    }
}

function redo() {
    if(redoStack.length > 0)
    {
        const nodes = redoStack.pop() 
        saveNodes(nodes, "redo")
    }
}

function getChildrensRec(node)
{
    let childs = []

    if(node.childs != undefined && node.childs.length != 0) {
        for(let i = 0; i < node.childs.length; i++)
        {
            childs.push(node.childs[i])
            childs = childs.concat(getChildrensRec(node.childs[i]))
        }
    }

    return childs
}

function getUnavailableNodesRec(currentNode, targetNode){
    let returnPackage = {}
    returnPackage.isParent = false
    returnPackage.unavailableNodes = []

    if(currentNode.id == targetNode.id) { 
        returnPackage.isParent = true
        returnPackage.unavailableNodes = getChildrensRec(currentNode)
        return returnPackage 
    }

    if(currentNode.childs == undefined || currentNode.childs.length == 0) {
        return returnPackage 
    }

    for(let i = 0; i < currentNode.childs.length; i++)
    {
        let childResult = getUnavailableNodesRec(currentNode.childs[i], targetNode)

        if(childResult.isParent) { returnPackage.isParent = true }

        returnPackage.unavailableNodes = returnPackage.unavailableNodes.concat(childResult.unavailableNodes)
    }

    if(returnPackage.isParent)
    {
        returnPackage.unavailableNodes = returnPackage.unavailableNodes.concat([currentNode])
    }

    return returnPackage
}

function refreshAvailableNodes() {
    let select = document.getElementById('node-select')
    select.innerHTML = ""

    let option = document.createElement('option')
    option.value = -1
    
    select.appendChild(option)

    if(selectedNode.length == 1)
    {
        const nodeId = parseInt(selectedNode[0].getAttribute("node-id"))
        const node = scene.get(nodeId)

        const root = scene.get(1)

        let availableNodes = Array.from(scene.values())
        let unavailableNodes = getUnavailableNodesRec(root, node).unavailableNodes

        availableNodes = availableNodes.filter(
            function(e) {
                return this.indexOf(e) < 0;
            },
            unavailableNodes
        )

        for(let i = 0; i < availableNodes.length; i++)
        {
            addNodeToSelect(availableNodes[i])
        }
    }
}

function addNodeToSelect(node) {
    let select = document.getElementById('node-select')
    let option = document.createElement('option')
    option.value = node.id
    option.innerText = '('+ node.id +') ' + node.name

    select.appendChild(option)
}

function updateNodeFromForm() {
    let updatedNodes = []
    let multipleSelect = selectedNode.length > 1

    for(let i = 0; i < selectedNode.length; i ++)
    {
        const nodeId = parseInt(selectedNode[i].getAttribute("node-id"))
        const node = scene.get(nodeId)

        let updatedNode = {};

        updatedNode.transformation = {}
        updatedNode.model = {}
    
        updatedNode.id = nodeId

        if(multipleSelect){
            updatedNode.name = node.name
        }
        else
        {
            updatedNode.name = document.getElementsByName("name-input")[0].value
        }
        
        updatedNode.model.geometryType = parseInt(document.getElementById("geometry-select").value)

        const geometryPathIsSet = document.getElementById("geometry-source-name").innerText != ""

        if(geometryPathIsSet)
        {
            updatedNode.model.geometryPath = document.getElementById("geometry-source-name").innerText

            const fileName = updatedNode.model.geometryPath
            const fileExt = fileName.split('.').pop()
            const modelFileTypeID = modelFileType.findIndex(function(e) { return e == fileExt })
    
            updatedNode.model.fileType = modelFileTypeID
        }

        updatedNode.transformation.translation = []
        updatedNode.transformation.rotation = []
        updatedNode.transformation.scale = []

        for(let j = 0; j < coordinates.length; j++)
        {
            let currentPosition
            let positionFieldValue = document.getElementsByName("position-input-"+coordinates[j])[0].value
            let positionIsSet = (positionFieldValue != "")
            currentPosition = positionIsSet ? parseFloat(positionFieldValue) : node.data.position[j]
            updatedNode.transformation.translation.push(currentPosition)

            let currentOrientation
            let orientationFieldValue = document.getElementsByName("rotation-input-"+coordinates[j])[0].value
            let orientationIsSet = (orientationFieldValue != "")
            currentOrientation = orientationIsSet ? parseFloat(orientationFieldValue) : node.data.orientation[j]
            updatedNode.transformation.rotation.push(currentOrientation)

            let currentSize
            let sizeFieldValue = document.getElementsByName("scale-input-"+coordinates[j])[0].value
            let sizeIsSet = (sizeFieldValue != "")
            currentSize = sizeIsSet ? parseFloat(sizeFieldValue) : node.data.size[j]
            updatedNode.transformation.scale.push(currentSize)
        }
        updatedNodes.push(updatedNode)
    }
    
    saveNodes(updatedNodes)
}

function createNodeLink(parent) {
    const parentId = parseInt(parent.getAttribute("node-id"))
    let parentNode = scene.get(parentId)

    ipc.invoke('create-node', JSON.stringify(parentId)).then((data) => {
        let newNode = JSON.parse(data)
        scene.set(parseInt(newNode.id), newNode)

        addNodeToView(newNode, scene.get(parentId))
        updateDisplayChilds(parent)
        if(parentNode.childs == undefined) { parentNode.childs = [] }
        parentNode.childs.push(newNode)

        scene.set(parseInt(parentNode.id), parentNode)
        refreshAvailableNodes()
    })
}

function addNodeLink(nodeId, parent)
{
    const parentId = parseInt(parent.getAttribute("node-id"))
    let parentNode = scene.get(parentId)
    let childNode = scene.get(nodeId)

    let dataPacket = {}

    dataPacket.parent = parentId
    dataPacket.node = nodeId

    parentNode.childs.push(childNode)
    ipc.invoke('link-node', JSON.stringify(dataPacket))

    let parentDomElements = document.getElementsByClassName('node-' + parentId)
    let nodeDom = createNodeTabWrapper(childNode)
    generateSceneViewRec(nodeDom, childNode)

    for (let parentDomElement of parentDomElements)
    {
        let childsWrapper = childrenMatches(parent, '.node-childrens')[0]
        childsWrapper.appendChild(nodeDom)
    }

    updateDisplayChilds(parent)
    refreshAvailableNodes()
    
    scene.set(parseInt(parentNode.id), parentNode)
}

function removeNodeLink(node) {
    const nodeId = parseInt(node.getAttribute("node-id"))
    const parentId = parseInt(node.parentNode.parentNode.getAttribute("node-id"))


    let dataPacket = {}
    dataPacket.parent = parentId
    dataPacket.node = nodeId

    let parentNode = scene.get(parentId)
    for (let i = 0; i < parentNode.childs.size; i++) {
        if (parentNode.childs[i].id == id) {
            parentNode.childs.splice(i, 1)
        }
    }

    ipc.invoke('unlink-node', JSON.stringify(dataPacket))
    ipc.invoke('get-nodes', false).then((data) => {
        let node = JSON.parse(data)
        scene = new Map()
        generateSceneRec(node, null)
    })

    node.parentNode.removeChild(node)
}

function editNodeLink() {
    const refNodeId = parseInt(document.getElementById('node-select').value)

    if(refNodeId != -1)
    {
        console.log("test")
        let parent = selectedNode[0].parentNode.parentNode
        removeNodeLink(selectedNode[0])
        addNodeLink(refNodeId, parent)
    }
}

function updateEditForm() {
    const node = scene.get(parseInt(selectedNode[0].getAttribute("node-id")))
    refreshAvailableNodes()

    let id = node.id
    let name = node.name
    let color = [
        1.0,
        1.0,
        1.0
    ]

    let geometryType = -1

    if(node.material != undefined)
    {
        color = node.material.color
    }

    if(node.model != undefined)
    {
        geometryType = node.model.geometryType     
    }

    let position = node.transformation.translation
    let orientation = node.transformation.rotation
    let size = node.transformation.scale

    for(let i = 1; i < selectedNode.length; i ++)
    {
        let currentNode = scene.get(parseInt(selectedNode[i].getAttribute("node-id")))
        name += " - " + currentNode.name
        if(texture != currentNode.texture) { texture = "" }
        if(color != currentNode.color) { color = [1,1,1] }
        if(id != currentNode.id) { id = -1 }
        if(geometrySource != currentNode.geometrySource) { geometrySource = -1 }

        for(let j = 0; j < 3; j++)
        {
            if( position[j] !== currentNode.data.position[j] ) { position[j] = "" }
            if( orientation[j] !== currentNode.data.orientation[j] ) { orientation[j] = "" }
            if( size[j] !== currentNode.data.size[j] ) { size[j] = "" }
        }
    }

    let hexColor = "";
    for(let i = 0; i < 3; i ++)
    {
        let currentColor = Math.floor((color[i] * 255)).toString(16)
        if (currentColor.length < 2)
        {
            currentColor += '0' 
        }

        hexColor = hexColor.concat(currentColor)
    }

    changeSelectedOption("node-select", id)
    changeSelectedOption("geometry-select", geometryType)
    document.getElementById("geometry-source-name").innerText = "";
    document.getElementById('color-picker').jscolor.fromString(hexColor);
    document.getElementsByName("name-input")[0].value = name;
    document.getElementById("texture-name").innerText = "";
    document.getElementById("normal-map-texture-name").innerText = "";

    for(let i = 0; i < 3; i++)
    {
        document.getElementsByName("position-input-"+coordinates[i])[0].value = position[i]
        document.getElementsByName("rotation-input-"+coordinates[i])[0].value = orientation[i]
        document.getElementsByName("scale-input-"+coordinates[i])[0].value = size[i]
    }
}

function createNodeTabWrapper(node)
{
    let nodeDom = document.createElement('div')
    nodeDom.className = (nodeClass + ' node-' + node.id)
    nodeDom.setAttribute('node-id', node.id)

    return nodeDom
}

function generateSceneViewRec(domElement, node)
{ 
    let nodeInfo = generateNodeInfoTab(node)
    let childsWrapper = document.createElement('div')
    childsWrapper.className = childsClass + ' hidden'

    //Recursive call
    if(node.childs != undefined)
    {
        node.childs.forEach((childNode) => {
            childDom = createNodeTabWrapper(childNode)
            childsWrapper.appendChild(childDom)
            generateSceneViewRec(childDom, childNode)
        })
    }

    domElement.appendChild(nodeInfo)
    domElement.appendChild(childsWrapper)
}

function generateNodeInfoTab(node)
{
    const isRoot = node.id == 1

    let nodeInfo = document.createElement('div')
    nodeInfo.className = 'node-info'

    let seeChildsTab = document.createElement("div")
    seeChildsTab.className = "glyph-icon flaticon-keyboard-right-arrow-button"
    seeChildsTab.style.width = "12px"

    let nameTab = document.createElement("div")
    nameTab.className = "node-name"
    nameTab.innerHTML = "<span>" + node.name + "</span>"

    let buttonsTab = document.createElement("div")
    buttonsTab.className = "buttons are-small node-btn-wrapper"

    let buttonAdd = document.createElement("button")
    buttonAdd.className = "button is-success is-inverted"

    let buttonAddIcon = document.createElement("i")
    buttonAddIcon.className = "glyph-icon flaticon-add-plus-button"
    buttonAdd.appendChild(buttonAddIcon)
    buttonsTab.appendChild(buttonAdd)

    if (!isRoot) {
        let buttonDelete = document.createElement("button")
        buttonDelete.className = "button is-danger is-inverted"
        let buttonDeleteIcon = document.createElement("i")
        buttonDeleteIcon.className = "glyph-icon flaticon-clear-button"
        buttonDelete.appendChild(buttonDeleteIcon)
        buttonsTab.appendChild(buttonDelete)

        buttonDelete.addEventListener('click', function (event) {
            const nodeDOM = this.parentNode.parentNode.parentNode
            removeNodeLink(nodeDOM)
        })
    }

    // Event listener initialisation
    buttonAdd.addEventListener('click', function (event) {
        const nodeDOM = this.parentNode.parentNode.parentNode
        createNodeLink(nodeDOM)
    })

    nameTab.addEventListener('click', function (event) {
        const multipleSelect = event.ctrlKey

        changeSelectedTab(this.parentNode.parentNode, multipleSelect)
        updateEditForm()
    })

    seeChildsTab.addEventListener('click', function (event) {
        toggleDisplayChilds(this)
        updateDisplayChilds(this.parentNode.parentNode)
    });

    nodeInfo.appendChild(seeChildsTab)
    nodeInfo.appendChild(nameTab)
    nodeInfo.appendChild(buttonsTab)

    return nodeInfo
}

function addNodeToView(node, parent) {
    let parentDomElements = []
    let rootElement = node.id == 1;

    if (!rootElement) {
        parentDomElements = Array.from(document.getElementsByClassName('node-' + parent.id));
    }
    else {
        parentDomElements[0] = document.getElementById("scene-graph")
    }

    parentDomElements.forEach((elem) => {
        let nodeDom = document.createElement("div")
        nodeDom.className = (nodeClass + " node-" + node.id)
        nodeDom.setAttribute("node-id", node.id)

        let childInfo = document.createElement("div")
        childInfo.className = "node-info"

        let childrens = document.createElement("div")
        childrens.className = childsClass + " hidden"

        let seeChildsTab = document.createElement("div")
        seeChildsTab.className = "glyph-icon flaticon-keyboard-right-arrow-button"
        seeChildsTab.style.width = "12px"

        let nameTab = document.createElement("div")
        nameTab.className = "node-name"
        nameTab.innerHTML = "<span>" + node.name + "</span>"

        let buttonsTab = document.createElement("div")
        buttonsTab.className = "buttons are-small node-btn-wrapper"

        let buttonAdd = document.createElement("button")
        buttonAdd.className = "button is-success is-inverted"
        let buttonAddIcon = document.createElement("i")
        buttonAddIcon.className = "glyph-icon flaticon-add-plus-button"
        buttonAdd.appendChild(buttonAddIcon)
        buttonsTab.appendChild(buttonAdd)

        if (!rootElement) {
            let buttonDelete = document.createElement("button")
            buttonDelete.className = "button is-danger is-inverted"
            let buttonDeleteIcon = document.createElement("i")
            buttonDeleteIcon.className = "glyph-icon flaticon-clear-button"
            buttonDelete.appendChild(buttonDeleteIcon)
            buttonsTab.appendChild(buttonDelete)

            buttonDelete.addEventListener('click', function (event) {
                const nodeDOM = this.parentNode.parentNode.parentNode
                removeNodeLink(nodeDOM)
            })
        }

        buttonAdd.addEventListener('click', function (event) {
            const nodeDOM = this.parentNode.parentNode.parentNode
            createNodeLink(nodeDOM)
        })

        childInfo.appendChild(seeChildsTab)
        childInfo.appendChild(nameTab)
        childInfo.appendChild(buttonsTab)
        nodeDom.appendChild(childInfo)
        nodeDom.appendChild(childrens)

        // Event listener initialisation
        nameTab.addEventListener('click', function (event) {
            const multipleSelect = event.ctrlKey

            changeSelectedTab(this.parentNode.parentNode, multipleSelect)
            updateEditForm()
        })

        seeChildsTab.addEventListener('click', function (event) {
            toggleDisplayChilds(this)
            updateDisplayChilds(this.parentNode.parentNode)
        });

        if (rootElement) {
            elem.appendChild(nodeDom)
        } else {
            let parentChildsWrapper = childrenMatches(elem, "." + childsClass)[0]
            parentChildsWrapper.appendChild(nodeDom);
        }
    })
}

function generateSceneRec(currentNode, parent) {
    scene.set(currentNode.id, currentNode)
    if (currentNode.childs !== undefined) {
        currentNode.childs.forEach(childNode => generateSceneRec(childNode, currentNode))
    }
}

function startRender(){
    let dataPacket = {}

    dataPacket.threadCount = parseInt(document.getElementsByName("threads-count")[0].value)
    dataPacket.samplesPerPixel = parseInt(document.getElementsByName("ite-per-thread")[0].value)
    dataPacket.maxDepth = parseInt(document.getElementsByName("max-depth")[0].value)
    dataPacket.savePath = document.getElementsByName("result-file-name")[0].value

    ipc.invoke('start-render', JSON.stringify(dataPacket)).then((data) => {})
}

function initGeometrySelect() {
    let select = document.getElementById("geometry-select")
    for(let i = 0; i < geometrySource.length; i++)
    {
        let option = document.createElement('option')
        option.value = i
        option.innerText = geometrySource[i]

        select.appendChild(option)
    }
}

function initMaterialSelect() {
    
    let select = document.getElementById("material-type-select")
    for(let i = 0; i < materialType.length; i++)
    {
        let option = document.createElement('option')
        option.value = i
        option.innerText = materialType[i].title

        select.appendChild(option)
    }

    updateMaterialForm(materialType[0])
}

function updateMaterialForm(selectedMaterialType) {
    let materialOptions = Array.from(document.getElementsByClassName("material-option"))

    let hideOptions = option => {
        if(option.id != selectedMaterialType.id + '-options')
            option.className = materialOptionsClass + ' hidden'
        else
            option.className = materialOptionsClass

        if (option.id == 'textureImage-options' && (selectedMaterialType.id != 'checkerboard'))
            option.className = materialOptionsClass
    }

    materialOptions.map(hideOptions)
}

function updateMaterial() {
    const data =  prepareMaterialUpdateData()
    saveMaterial(data)
}

function prepareMaterialUpdateData() {
    let updatedNodesMaterial = []
    let texturePath;
    let normalMap;

    const hexColor = document.getElementById("color-picker").innerHTML

    color = [0.0, 0.0, 0.0]
    for(let i = 0; i < 3; i ++)
    {
        color[i] = (parseInt(hexColor.charAt(i * 2) + hexColor.charAt(i * 2 + 1), 16) / 255)
        if (!color[i]) { color[i] = 0.0 }
    }

    const textureIsSet = document.getElementById("texture-name").innerText != ""
    if(textureIsSet)
        texturePath = document.getElementById("texture-name").innerText;

    const normalMapIsSet = document.getElementById("normal-map-texture-name").innerText != ""
    if(normalMapIsSet)
        normalMap = document.getElementById("normal-map-texture-name").innerText;
    
    for(let i = 0; i < selectedNode.length; i ++)
    {
        let updatedNodeMaterial = {};
        const nodeId = parseInt(selectedNode[i].getAttribute("node-id"))

        updatedNodeMaterial.materialType = parseInt(document.getElementById("material-type-select").value)
        updatedNodeMaterial.color = color
        updatedNodeMaterial.nodeId = nodeId

        if (textureIsSet)
            updatedNodeMaterial.texture = texturePath

        if (normalMapIsSet)
            updatedNodeMaterial.normalMap = normalMap

        const selectedMaterialType = materialType[updatedNodeMaterial.materialType]
        switch(selectedMaterialType.id)
        {
            case 'diffLight':
                updatedNodeMaterial.intensity = parseFloat(document.getElementsByName("luminosity-value")[0].value)
                break
            case 'metalic':
                updatedNodeMaterial.fuzz = parseFloat(document.getElementsByName("fuzz-value")[0].value)
                break
            case 'dielectric':
                updatedNodeMaterial.refIndex = parseFloat(document.getElementsByName("refraction-index-value")[0].value)
            default:
                break
        }
        
        updatedNodesMaterial.push(updatedNodeMaterial)
    }

    return updatedNodesMaterial
}

function saveMaterial(materials) {
    materials.forEach(material => {
        const dataPacket = JSON.stringify(material)
        console.log(dataPacket)
        ipc.invoke('update-material', dataPacket)
        ipc.invoke('get-node', material.nodeId).then((data) => {
            let returnNode = JSON.parse(data)
            scene.set(material.nodeId, returnNode);

            let tabs = document.getElementsByClassName('node-'+material.nodeId)
            
            for(let tab of tabs)
            {
                let name = childrenMatches(childrenMatches(tab, '.node-info')[0], '.node-name')[0].firstChild
                name.innerText = returnNode.name
            }
   
            updateEditForm()
            refreshAvailableNodes()
            updateEditForm()
        })
    });
}

function initUndoRedoListener() {
    document.addEventListener('keydown', function(e) {
        if (e.ctrlKey)
        {
            if(e.code == "KeyZ")
            {
                e.preventDefault()
                if(e.shiftKey)
                {
                    redo()
                }
                else
                {
                    undo()
                }
            }
        }
    }, true)
}

function updateCamera() {
    let dataPacket = {}

    dataPacket.projectionType = camera.projectionType
    dataPacket.vfov = parseInt(document.getElementsByName('camera-vfov')[0].value)
    
    dataPacket.lookFrom = []
    dataPacket.lookAt = []
    for(let i = 0; i < 3; i++)
    {
        dataPacket.lookFrom[i] = parseInt(document.getElementsByName("look-from-input-" + coordinates[i])[0].value)
        dataPacket.lookAt[i] = parseInt(document.getElementsByName("look-at-input-" + coordinates[i])[0].value)
    }
    
    ipc.invoke('update-camera', JSON.stringify(dataPacket)).then((data) => {
        ipc.invoke('get-camera').then((data) => {
            camera = JSON.parse(data)
        })
    })
}

function updateCameraForm() {
    document.getElementsByName('camera-projection-type').value = camera.projectionType
    document.getElementsByName('camera-vfov')[0].value = camera.vfov

    for(let i = 0; i < 3; i++)
    {
        document.getElementsByName("look-from-input-" + coordinates[i])[0].value = camera["lookFrom"][i]
        document.getElementsByName("look-at-input-" + coordinates[i])[0].value = camera["lookAt"][i]
    }
    

}

function initCamera() {
    ipc.invoke('get-camera').then((data) => {
        camera = JSON.parse(data) 
        updateCameraForm()
    })

    let projectionTypesRdo = document.getElementsByName("camera-projection-type")
    projectionTypesRdo.forEach(elem => elem.addEventListener('click', function (event) {
        camera.projectionType = parseInt(this.value)
    }))

    document.getElementById("edit-camera").addEventListener('click', function (event) {
        updateCamera();
    })
}

function init() {
    
    ipc.invoke('get-nodes', false).then((data) => {
        let node = JSON.parse(data)
        
        generateSceneRec(node, null)
        let root = document.getElementById('scene-graph')
        let rootNode = document.createElement('div')
        rootNode.className = (nodeClass + " node-1")
        rootNode.setAttribute("node-id", 1)

        root.appendChild(rootNode)

        generateSceneViewRec(rootNode, node)
        refreshAvailableNodes()
    })

    document.getElementsByName("texture")[0].addEventListener('change', function (event) {
        let fileName = document.getElementById("texture-name")
        fileName.innerText = this.files[0].path;
    })

    document.getElementsByName("normal-map-texture")[0].addEventListener('change', function (event) {
        let fileName = document.getElementById("normal-map-texture-name")
        fileName.innerText = this.files[0].path;
    })

    document.getElementsByName("geometry-source")[0].addEventListener('change', function (event) {
        let fileName = document.getElementById("geometry-source-name")
        fileName.innerText = this.files[0].path;
    })

    document.getElementById("material-type-select").addEventListener('change', function (event) {
        updateMaterialForm(materialType[this.value])
    })

    document.getElementById("edit-node").addEventListener('click', function (event) {
        updateNodeFromForm();
    })

    document.getElementById("edit-node-ref").addEventListener('click', function (event) {
        editNodeLink();
    })

    document.getElementById("render-scene").addEventListener('click', function (event) {
        startRender();
    })

    document.getElementById("edit-material").addEventListener('click', function (event) {
        updateMaterial();
    })

    initCamera()
    initGeometrySelect()
    initMaterialSelect()
    initUndoRedoListener()
}


init();