#pragma once
#include <vulkan/vulkan.h>
#include <stdexcept>
#include "Device.h"

class Buffer
{
protected:
	Device* device;
	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;

	Buffer(Device* device)
	{
		this->device = device;

		stagingBuffer = VkBuffer();
		stagingBufferMemory = VkDeviceMemory();
	}

	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory);
	void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
	VkCommandBuffer beginSingleTimeCommands();
	void endSingleTimeCommands(VkCommandBuffer commandBuffer);
	uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);
};

