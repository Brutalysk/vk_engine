#pragma once
#include "HittableList.h"
#include "RtTriangle.h"
#include <algorithm>

class BvhNode : public Hittable
{
public:
	shared_ptr<Hittable> left;
	shared_ptr<Hittable> right;
	aabb box;

    BvhNode();

    BvhNode(HittableList& list)
        : BvhNode(list.objects, 0, list.objects.size())
    {}

    BvhNode(std::vector<shared_ptr<Hittable>>& objects, size_t start, size_t end);

    virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
    virtual bool bounding_box(aabb& output_box) const;
};

