#include "Camera.h"

json Camera::jsonEncode()
{
    json data;
     
    data["id"] = this->id;
    data["name"] = this->name;
    data["projectionType"] = this->projectionType;
    data["vfov"] = this->vfov;

    for(size_t i = 0; i < 3; i++)
    {
        data["vup"][i] = this->vup[i];
        data["lookAt"][i] = this->lookAt[i];
        data["lookFrom"][i] = this->lookFrom[i];
    }

    return data;
}

void Camera::updateFromJson(json data)
{
    if(data["name"] != nullptr)
        this->name = data["name"];

    if(data["vfov"] != nullptr)
        this->vfov = data["vfov"];

    if(data["projectionType"] != nullptr)
        this->projectionType = data["projectionType"];

    if(data["aspect"] != nullptr)
        this->aspect = data["aspect"];

    if(data["vup"] != nullptr)
    {
        for(size_t i = 0; i < 3; i++)
        {
            this->vup[i] = data["vup"][i];
        }
    }

    if(data["lookFrom"] != nullptr && data["lookAt"] != nullptr)
    { 
        for(size_t i = 0; i < 3; i++)
        {
            this->lookFrom[i] = data["lookFrom"][i];
            this->lookAt[i] = data["lookAt"][i];
        }
    }

    updateViewMat();
}

void Camera::updateViewMat(){
    auto theta = glm::radians(vfov);
    auto half_height = tan(theta / 2);
    auto half_width = aspect * half_height;
    
    glm::vec3 u, v, w;
    w = glm::normalize(lookFrom - lookAt);
    u = glm::normalize(glm::cross(vup, w));
    v = glm::cross(w, u);
    
    lowerLeftCorner = lookFrom - glm::vec3(half_width) * u - glm::vec3(half_height) * v - w;

    horizontal = glm::vec3(2 * half_width) * u;
    vertical = glm::vec3(2 * half_height) * v;

    view = glm::lookAt(lookFrom, lookAt, vup);
}

Ray Camera::getRay(double u, double v) {
    return Ray(lookFrom, lowerLeftCorner + glm::vec3(u) * horizontal + glm::vec3(v) * vertical - lookFrom);
}