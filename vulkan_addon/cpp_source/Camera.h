#ifndef VKMASSIVE_CAMERA_H
#define VKMASSIVE_CAMERA_H

#include <iostream>
#include <nlohmann/json.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Ray.h"

using json = nlohmann::json;

enum CameraProjectionType {
    PERSPECTIVE,
    ORTHOGONAL
};

class Camera
{
public:
	unsigned long long id;
	std::string name;
    CameraProjectionType projectionType;
	glm::vec3 lookFrom;
	glm::vec3 lookAt;
	glm::vec3 vup;
	double aspect;
	float vfov;

	glm::mat4 view;
	glm::vec3 lowerLeftCorner;
	glm::vec3 horizontal;
	glm::vec3 vertical;

	json jsonEncode();
	void updateFromJson(json data);

	Camera(
		glm::vec3 lookfrom, glm::vec3 lookat, glm::vec3 vup,
		float vfov, double aspect) {

		this->aspect = aspect;
		this->vfov = vfov;
		this->lookFrom = lookfrom;
		this->lookAt = lookat;
		this->vup = vup;

		glm::vec3 u, v, w;

		auto theta = glm::radians(vfov);
		auto half_height = tan(theta / 2);
		auto half_width = aspect * half_height;
		w = glm::normalize(lookfrom - lookat);
		u = glm::normalize(glm::cross(vup, w));
		v = glm::cross(w, u);

		lowerLeftCorner = lookFrom - glm::vec3(half_width) * u - glm::vec3(half_height) * v - w;

		horizontal = glm::vec3(2 * half_width) * u;
		vertical = glm::vec3(2 * half_height) * v;

		view = glm::lookAt(lookfrom, lookat, vup);
	}

	Ray getRay(double u, double v);

private:
	void updateViewMat();
};

#endif