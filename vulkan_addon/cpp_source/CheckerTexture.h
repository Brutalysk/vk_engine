#pragma once

#include <memory>
#include "Texture.h"
class CheckerTexture : public Texture
{
public:
	std::shared_ptr<Texture> odd;
	std::shared_ptr<Texture> even;

	CheckerTexture() {}
	CheckerTexture(std::shared_ptr<Texture> t0, std::shared_ptr<Texture> t1)
	{
		this->even = t0;
		this->odd = t1;
	}

	virtual glm::vec3 value(double u, double v, const glm::vec3& p) const {
		auto sines = sin(10 * p.x) * sin(10 * p.y) * sin(10 * p.z);
		if (sines < 0)
			return odd->value(u, v, p);
		else
			return even->value(u, v, p);
	}
};

