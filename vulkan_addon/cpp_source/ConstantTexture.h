#pragma once
#include "Texture.h"
#include <glm\glm.hpp>

class ConstantTexture : public Texture
{
public:
    glm::vec3 color;

    ConstantTexture() {}
    ConstantTexture(glm::vec3 c)
    {
        color = c;
    }

    virtual glm::vec3 value(double u, double v, const glm::vec3& p) const {
        return color;
    }
};

