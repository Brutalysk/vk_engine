#pragma once
#include <vulkan/vulkan.h>

class DepthImage
{
public:
	VkImage depthImage;
	VkDeviceMemory depthImageMemory;
	VkImageView	depthImageView;
};

