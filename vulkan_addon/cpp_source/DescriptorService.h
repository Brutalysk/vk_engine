#ifndef VKMASSIVE_DESCRIPTOR_SERVICE_H
#define VKMASSIVE_DESCRIPTOR_SERVICE_H

#include <vector>
#include <array>
#include <vulkan/vulkan.h>

#include "Material.h"
#include "Device.h"
#include "SwapChain.h"
#include "UniformBufferObject.h"
#include "ModelRenderInfo.h"
#include "Node.h"

class DescriptorService
{
private:
	Device* device;
	size_t imageCount;

	void createDescriptorSetLayouts();

	void createCombinedDescriptorSets(shared_ptr<ModelRenderInfo>, size_t);
	void createDescriptorPools(shared_ptr<ModelRenderInfo>, size_t);

public:
	VkDescriptorSetLayout descriptorSetLayout;

	void updateTextureDescriptorSet(Texture* texture);
	void updateUboDescriptorSet(VkBuffer uniformBuffer);

	void createDescriptorSets(std::vector<shared_ptr<ModelRenderInfo>>, size_t);
	void deleteDescriptorSets();

	DescriptorService(Device* device, size_t imageCount)
	{
		this->device = device;
		this->imageCount = imageCount;

		createDescriptorSetLayouts();
	}
};

#endif