#pragma once
#include <vulkan/vulkan.h>
#include <stdexcept>
#include <vector>
#include <set>
#include "QueueFamilyIndices.h"

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation"
};


class Device
{
public:
	VkQueue graphicsQueue;
	VkQueue presentQueue;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VkDevice vkDevice; 
	VkCommandPool commandPool;

	QueueFamilyIndices findQueueFamilies(VkPhysicalDevice, VkSurfaceKHR);

	Device(VkInstance instance, VkSurfaceKHR surface, bool enableValidationLayers) {
		this->surface = surface;
		this->enableValidationLayers = enableValidationLayers;

		this->pickPhysicalDevice(instance);
		this->createLogicalDevice();
		this->createCommandPool();
	}

	void destroyDevice();

private:
	VkSurfaceKHR surface;
	bool enableValidationLayers;

	const std::vector<const char*> deviceExtensions = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	void pickPhysicalDevice(VkInstance instance);
	void createLogicalDevice();

	// Helper functions
	bool isDeviceSuitable(VkPhysicalDevice);
	bool checkDeviceExtensionSupport(VkPhysicalDevice);
	SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice);
	void createCommandPool();
};

