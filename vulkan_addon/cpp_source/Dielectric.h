#pragma once
#include "Material.h"

class Dielectric : public Material
{
public:
    Dielectric(glm::vec3& a, double ri, shared_ptr<Texture> texture) : Material(a) {
        ref_idx = ri;
        this->texture = texture;
    }
        

    virtual bool scatter(
        const Ray& r_in, const hit_record& rec, glm::vec3& attenuation, Ray& scattered
    ) const {
        attenuation = glm::vec3(1.0, 1.0, 1.0);
        float etai_over_etat = (rec.front_face) ? (1.0 / ref_idx) : (ref_idx);

        glm::vec3 unit_direction = glm::normalize(r_in.direction);
        double cos_theta = Utils::ffmin(glm::dot(-unit_direction, rec.normal), 1.0);
        double sin_theta = sqrt(1.0 - cos_theta * cos_theta);
        if (etai_over_etat * sin_theta > 1.0) {
            glm::vec3 reflected = glm::reflect(unit_direction, rec.normal);
            scattered = Ray(rec.p, reflected);
            return true;
        }
        double reflect_prob = Utils::schlick(cos_theta, etai_over_etat);
        if (Utils::random_double() < reflect_prob)
        {
            glm::vec3 reflected = glm::reflect(unit_direction, rec.normal);
            scattered = Ray(rec.p, reflected);
            return true;
        }

        glm::vec3 refracted = glm::refract(unit_direction, rec.normal, etai_over_etat);
        scattered = Ray(rec.p, refracted);
        return true;
    }

    double ref_idx;
};

