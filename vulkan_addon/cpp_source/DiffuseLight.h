#pragma once
#include "Material.h"
class DiffuseLight : public Material
{
public:
	DiffuseLight(glm::vec3& a, shared_ptr<Texture> texture) : Material(a) {
		this->texture = texture;
	}

	virtual bool scatter(
		const Ray& r_in, const hit_record& rec, glm::vec3& attenuation, Ray& scattered
	) const {
		return false;
	}

	virtual glm::vec3 emitted(double u, double v, const glm::vec3& p) const {
		//return glm::vec3(u, v, u - v);
		return colorFilter * texture->value(u, v, p);
	}
};

