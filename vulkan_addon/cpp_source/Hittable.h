#ifndef HITTABLE_H
#define HITTABLE_H

#include "Ray.h"
#include "Utils.h"
#include "aabb.h"

class Material;

struct hit_record {
    double t;
    double u;
    double v;
    glm::vec3 p;
    glm::vec3 normal;
    std::shared_ptr<Material> mat_ptr;
    bool front_face;

    inline void set_face_normal(const Ray& r, const glm::vec3& outward_normal) {
        front_face = glm::dot(r.direction, outward_normal) < 0;
        normal = front_face ? outward_normal : -outward_normal;
    }
};

class Hittable
{
public:
    virtual bool hit(const Ray& r, double t_min, double t_max, hit_record& rec) const = 0;
    virtual bool bounding_box(aabb& box) const = 0;
};

#endif