#include "HittableList.h"

bool HittableList::hit(const Ray& r, double t_min, double t_max, hit_record& rec) const {
    hit_record temp_rec;
    bool hit_anything = false;
    auto closest_so_far = t_max;

    for (const auto& object : objects) {
        if (object->hit(r, t_min, closest_so_far, temp_rec)) {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }

    return hit_anything;
}

bool HittableList::bounding_box(aabb& output_box) const
{
    if (objects.empty()) return false;

    aabb temp_box;
    bool first_true = objects[0]->bounding_box(temp_box);

    if (!first_true)
        return false;

    output_box = temp_box;

    for (const auto& object : objects) {
        if (!object->bounding_box(temp_box))
            return false;
        output_box = aabb::surrounding_box(output_box, temp_box);
    }

    return true;
}