#ifndef HITTABLE_LIST_H
#define HITTABLE_LIST_H

#include "Hittable.h"
#include <memory>
#include <vector>

using std::shared_ptr;
using std::make_shared;

class HittableList : public Hittable
{
public:
	std::vector<shared_ptr<Hittable>> objects;
	aabb box;

	void clear() { objects.clear(); }
	void add(shared_ptr<Hittable> object) { objects.push_back(object); }
	virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
	virtual bool bounding_box(aabb& box) const;
};

#endif