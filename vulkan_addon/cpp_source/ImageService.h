#ifndef VKMASSIVE_IMAGE_SERVICE_H
#define VKMASSIVE_IMAGE_SERVICE_H

#include <vulkan/vulkan.h>
#include <memory>
#include <stb_image.h>

#include "Device.h"
#include "Buffer.h"
#include "DepthImage.h"
#include "SwapChain.h"
#include "ImageTexture.h"

class SwapChain;

class ImageService : Buffer
{
public:
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
	void createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);
	void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
	bool hasStencilComponent(VkFormat format);
	void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
	VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);

	std::shared_ptr<ImageTexture> createTexture(const char* path);
	void createTextureImage(std::shared_ptr<ImageTexture> texture, const char* path);
	void createTextureImageView(std::shared_ptr<ImageTexture> texture);
	void createTextureSampler(std::shared_ptr<ImageTexture> texture);
	
	DepthImage createDepthResource(SwapChain* swapChain);
	VkFormat findDepthFormat();

	ImageService(Device* device)
	: Buffer(device) 
	{
		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
	}

	void cleanupTexture(std::shared_ptr<ImageTexture> texture);
	void cleanupDepthImage(DepthImage depthImage);
};

#endif