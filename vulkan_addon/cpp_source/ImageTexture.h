#pragma once
#include "Texture.h"
class ImageTexture : public Texture
{
public:
	unsigned char* data;
	int nx, ny;

	ImageTexture() {};
	ImageTexture(unsigned char* pixels, int A, int B)
	{
		this->data = pixels;
		this->nx = A;
		this->ny = B;
	}

	~ImageTexture() {
		delete data;
	}

    virtual glm::vec3 value(double u, double v, const glm::vec3& p) const {
        u = std::fmod(u, 1);
        v = std::fmod(v, 1);

        if (data == nullptr)
            return glm::vec3(0, 1, 1);

        auto i = static_cast<int>(u * nx);
        auto j = static_cast<int>(v * ny - 0.001);

        if (i < 0) i = 0;
        if (j < 0) j = 0;
        if (i > nx - 1) i = nx - 1;
        if (j > ny - 1) j = ny - 1;

        auto r = static_cast<int>(data[3 * i + 3 * nx * j + 0]) / 255.0;
        auto g = static_cast<int>(data[3 * i + 3 * nx * j + 1]) / 255.0;
        auto b = static_cast<int>(data[3 * i + 3 * nx * j + 2]) / 255.0;
        
        return glm::vec3(r, g, b);
    }
};

