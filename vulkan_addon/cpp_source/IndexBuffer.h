#pragma once
#include "Buffer.h"
#include <vulkan/vulkan.h>

class IndexBuffer : Buffer
{
public:
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;

	IndexBuffer(Device* device, std::vector<uint32_t> indices)
		:Buffer(device) 
	{
		VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

		VkBuffer stagingBuffer;
		VkDeviceMemory stagingBufferMemory;
		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

		void* data;
		vkMapMemory(device->vkDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
		memcpy(data, indices.data(), (size_t)bufferSize);
		vkUnmapMemory(device->vkDevice, stagingBufferMemory);

		createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

		copyBuffer(stagingBuffer, indexBuffer, bufferSize);

		vkDestroyBuffer(device->vkDevice, stagingBuffer, nullptr);
		vkFreeMemory(device->vkDevice, stagingBufferMemory, nullptr);
	}

	~IndexBuffer()
	{
		vkDestroyBuffer(device->vkDevice, indexBuffer, nullptr);
		vkFreeMemory(device->vkDevice, indexBufferMemory, nullptr);
	}
};