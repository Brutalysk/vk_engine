#pragma once
#include "Material.h"

class Lambertian : public Material {
public:
    Lambertian(glm::vec3& a, shared_ptr<Texture> texture) : Material(a)
    {
        this->texture = texture;
    }

    virtual bool scatter(
        const Ray& r_in, const hit_record& rec, glm::vec3& attenuation, Ray& scattered
    ) const {
        glm::vec3 scatter_direction = rec.normal + Utils::random_unit_vector();
        scattered = Ray(rec.p, scatter_direction);
        //attenuation = glm::vec3({ rec.u/2, rec.v/2, 0 });
        attenuation = colorFilter * texture->value(rec.u, rec.v, rec.p);
        return true;
    }
};
