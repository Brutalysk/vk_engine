#include "Material.h"

json Material::jsonEncode() {
	json data;

    data["color"][0] = this->colorFilter.r;
    data["color"][1] = this->colorFilter.g;
    data["color"][2] = this->colorFilter.b;

    return data;
}

void Material::updateFromJson(json data) {
    this->colorFilter = {
        data["color"][0],
        data["color"][1],
        data["color"][2]
    };
}


glm::vec3 Material::getNormal(double u, double v, glm::vec3& p) {
    if (this->normalMap != nullptr) 
        return this->normalMap->value(u, v, p) * glm::mat3(2.0) - glm::vec3(1);
    else
        return {0, 0, 1};
}