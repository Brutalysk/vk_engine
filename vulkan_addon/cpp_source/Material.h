#pragma once
#include <glm/glm.hpp>
#include <string>
#include <nlohmann/json.hpp>
#include "Texture.h"
#include "Ray.h"
#include "Hittable.h"

using json = nlohmann::json;

enum class MaterialType {
	LAMBERTIAN,
	DIFF_LIGHT,
	DIELECTRIC,
	METALIC,
	CHECKERBOARD
};

class Material
{
public:
	shared_ptr<Texture> texture;
	shared_ptr<Texture> normalMap;
	glm::vec3 colorFilter;

	json jsonEncode();
	void updateFromJson(json);

	glm::vec3 Material::getNormal(double u, double v, glm::vec3& p);

	virtual glm::vec3 emitted(double u, double v, const glm::vec3& p) const {
		return glm::vec3(0, 0, 0);
	}

	virtual bool scatter(const Ray& r_in, const hit_record& rec, glm::vec3& attenuation, Ray& scattered) const = 0;

	Material() : colorFilter(glm::vec3(1.0f)) {}	
	Material(glm::vec3 color) : colorFilter(color) {}
};