#pragma once
#include "Material.h"

class Metal : public Material {
public:
    Metal(const glm::vec3& a, double f, shared_ptr<Texture> texture) : Material(a) {
        fuzz = f < 1 ? f : 1;
        this->texture = texture;
    } 

    virtual bool scatter(
        const Ray& r_in, const hit_record& rec, glm::vec3& attenuation, Ray& scattered
    ) const {
        glm::vec3 reflected = reflect(glm::normalize(r_in.direction), rec.normal);
        scattered = Ray(rec.p, reflected + glm::vec3(fuzz) * Utils::random_in_unit_sphere());
        attenuation = colorFilter;
            
        return (glm::dot(scattered.direction, rec.normal) > 0);
    }

public:
    double fuzz;
};
