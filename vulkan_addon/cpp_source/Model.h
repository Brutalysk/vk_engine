#ifndef VKMASSIVE_MODEL_H
#define VKMASSIVE_MODEL_H

#include <vector>
#include "Vertex.h"
#include "GeometryType.h"
#include <nlohmann/json.hpp>
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "UniformBuffer.h"

using json = nlohmann::json;

class Model
{
public:
	std::vector<Vertex> vertices;
	std::vector<uint32_t> indices;


	size_t vertexOffset;

	GeometryType geometryType;

	json jsonEncode();
	void updateFromJson(json);

	Model(Device* device, size_t viewPortCount, size_t swapChainImagesCount) {
		this->device = device;
	}
private:
	Device* device;
};

#endif