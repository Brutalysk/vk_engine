#include "ModelFactory.h"

const float red = 1.0f;
const float green = 1.0f;
const float blue = 1.0f;

void ModelFactory::createCube(Model* cube)
{
    cube->vertices = {
        {{-0.5f, -0.5f, 0.5f}, {red, green, blue}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, 0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, 0.5f, 0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{-0.5f, -0.5f, 0.5f}, {red, green, blue}, {0.0f, 0.0f}},

        {{0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{-0.5f, 0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, 0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},

        {{0.5f, 0.5f, 0.5f}, {red, green, blue}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{0.5f, 0.5f, 0.5f}, {red, green, blue}, {0.0f, 0.0f}},

        {{-0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, 0.5f, 0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{-0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},

        {{-0.5f, -0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},
        {{0.5f, -0.5f, -0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, -0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, -0.5f, 0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{-0.5f, -0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},

        {{0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},
        {{-0.5f, 0.5f, -0.5f}, {red, green, blue}, {2.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{-0.5f, 0.5f, 0.5f}, {red, green, blue}, {2.0f, 2.0f}},
        {{0.5f, 0.5f, 0.5f}, {red, green, blue}, {0.0f, 2.0f}},
        {{0.5f, 0.5f, -0.5f}, {red, green, blue}, {0.0f, 0.0f}},
    };
    cube->geometryType = GeometryType::CUBE;
    setIndices(cube);
}

void ModelFactory::createPyramid(Model* pyramid)
{
    pyramid->vertices = {
        {{-0.5f, 0.0f, -0.5f}, {red, green, blue}, {1.0f, 0.0f}},
        {{0.0f, 1.0f, 0.0f}, {red, green, blue}, {1.0f, 1.0f}},
        {{0.5f, 0.0f, -0.5f}, {red, green, blue}, {0.0f, 1.0f}},

        {{0.0f, 0.0f, 0.5f}, {red, green, blue}, {1.0f, 0.0f}},
        {{0.0f, 1.0f, 0.0f}, {red, green, blue}, {1.0f, 1.0f}},
        {{-0.5f, 0.0f, -0.5f}, {red, green, blue}, {1.0f, 0.0f}},

        {{0.5f, 0.0f, -0.5f}, {red, green, blue}, {0.0f, 1.0f}},
        {{0.0f, 1.0f, 0.0f}, {red, green, blue}, {1.0f, 1.0f}},
        {{0.0f, 0.0f, 0.5f}, {red, green, blue}, {1.0f, 0.0f}},

        {{-0.5f, 0.0f, -0.5f}, {red, green, blue}, {1.0f, 0.0f}},
        {{0.0f, 0.0f, 0.5f}, {red, green, blue}, {1.0f, 0.0f}},
        {{0.5f, 0.0f, -0.5f}, {red, green, blue}, {0.0f, 1.0f}},
    };

    pyramid->geometryType = GeometryType::PYRAMID;
    setIndices(pyramid);
}

void ModelFactory::createSphere(Model* sphere)
{
    const double t = (1 + sqrt(5.0)) / 2.0;
    double rScale = 0.5 / sqrt(t * t + 1.0);
    sphere->vertices = std::vector<Vertex>(12);

    for (int i = 0; i < 4; i++)
        sphere->vertices[i].pos = glm::vec3(rScale) * glm::vec3({ 0, i & 2 ? -1 : 1, i & 1 ? -t : t });

    for (int i = 4; i < 8; i++)
        sphere->vertices[i].pos = glm::vec3(rScale) * glm::vec3(i & 2 ? -1 : 1, i & 1 ? -t : t, 0);

    for (int i = 8; i < 12; i++)
        sphere->vertices[i].pos = glm::vec3(rScale) * glm::vec3(i & 1 ? -t : t, 0, i & 2 ? -1 : 1);

    std::vector<std::vector<Vertex>> mesh;
    sphere->indices = {
        8, 2, 0,
        4, 8, 0,
        6, 4, 0,
        9, 6, 0,
        2, 9, 0,

        5, 7, 2,
        8, 5, 2,
        7, 9, 2,

        10, 5, 8,
        4, 10, 8,

        3, 5, 10,
        1, 3, 10,
        4, 1, 10,

        4, 6, 1,
        11, 3, 1,
        6, 11, 1,

        9, 11, 6,

        7, 3, 11,
        9, 7, 11,

        7, 5, 3
    };

    for (int i = 0; i < sphere->vertices.size(); i++)
    {
        double x = 0;
        double y = 0;
        if (i % 3 == 1)
            x, y = 0;

        if (i % 3 == 2) {
            x = 0.5;
            y = 1;
        }

        if (i % 3 == 0) {
            x = 1;
            y = 0;
        }

        sphere->vertices[i].texCoord = { x, y };
    }

    sphere->geometryType = GeometryType::SPHERE;
}

void ModelFactory::getShape(GeometryType objectType, Model* model)
{
    switch (objectType)
    {
    case (GeometryType::CUBE):
        createCube(model);
        break;
    case (GeometryType::PYRAMID):
        createPyramid(model);
        break;
    case(GeometryType::SPHERE):
        createSphere(model);
        break;
    //case (GeometryType::LINDENMAYER_TREE):
        //model = createLindenmayerForm(device);
        //break;
    default:
        throw std::runtime_error("failed to create requested primitive !");
    }
}


// Helper functions section start

void ModelFactory::setIndices(Model* model)
{
    model->indices.resize(model->vertices.size());

    for (int i = 0; i < model->vertices.size(); i++)
    {
        model->indices[i] = i;
    }
}