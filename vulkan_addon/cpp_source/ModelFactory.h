#ifndef VKMASSIVE_MODEL_FACTORY_H
#define VKMASSIVE_MODEL_FACTORY_H

#include "Node.h"
#include "Model.h"
#include "Vertex.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include "GeometryType.h"
#include "Device.h"

#define _USE_MATH_DEFINES
#include <math.h>

class ModelFactory
{
private:
	static std::shared_ptr<Node> createLindenmayerFormRec(int, Device*);
	static void createCube(Model*);
	static void createPyramid(Model*);
	static void createSphere(Model*);
	static Model* createLindenmayerForm(Model*);
	static void setIndices(Model*);
public:
	static void getShape(GeometryType, Model*);
};

#endif