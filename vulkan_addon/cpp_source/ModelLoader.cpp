#include "ModelLoader.h"

void ModelLoader::loadModelFromFbxFile(std::string filePath, Model* model)
{
    std::vector<Vertex> vertices = {};
    std::vector<uint32_t> indices = {};

    std::unordered_map<Vertex*, uint32_t> uniqueVertices = {};
    ofbx::IScene* scene = nullptr;

    FILE* fp = fopen(filePath.c_str(), "rb");
    fseek(fp, 0, SEEK_END);
    long file_size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    auto* content = new ofbx::u8[file_size];
    fread(content, 1, file_size, fp);

    scene = ofbx::load((ofbx::u8*)content, file_size, (ofbx::u64)ofbx::LoadFlags::TRIANGULATE);
    int mesh_count = scene->getMeshCount();

    for (int i = 0; i < mesh_count; ++i)
    {
        const ofbx::Mesh& mesh = *scene->getMesh(i);
        const ofbx::Geometry& geom = *mesh.getGeometry();
        int vertex_count = geom.getVertexCount();
        const ofbx::Vec3* currentVertices = geom.getVertices();
        const ofbx::Vec3* normals = geom.getNormals();

        for (int i = 0; i < vertex_count; ++i)
        {
            Vertex* vertex = new Vertex();
            vertex->pos = {
                currentVertices[i].x,
                currentVertices[i].y,
                currentVertices[i].z
            };

            vertex->color = {
                1.0f,
                1.0f,
                1.0f
            };

            vertex->texCoord = {
                1.0f,
                1.0f
            };

            vertex->normal = {
                normals[i].x,
                normals[i].y,
                normals[i].z
            };

            if (uniqueVertices.count(vertex) == 0) {
                uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(*vertex);
            }

            indices.push_back(uniqueVertices[vertex]);
        }
    }

    model->geometryType = GeometryType::MODEL;
    model->vertices = vertices;
    model->indices = indices;
}

void ModelLoader::loadModelFromStlFile(std::string filePath, Model* model)
{
    std::vector<Vertex> vertices = {};
    std::vector<uint32_t> indices = {};

    try {
        stl_reader::StlMesh <float, unsigned int> mesh(filePath);
        std::unordered_map<Vertex*, uint32_t> uniqueVertices = {};

        for (size_t itri = 0; itri < mesh.num_tris(); ++itri) {
            const float* normal = mesh.tri_normal(itri);

            for (size_t icorner = 0; icorner < 3; ++icorner) {
                Vertex* vertex = new Vertex();
                const float* c = mesh.tri_corner_coords(itri, icorner);

                vertex->pos = {
                    c[0],
                    c[1],
                    c[2]
                };

                vertex->color = {
                    1.0f,
                    1.0f,
                    1.0f
                };

                vertex->texCoord = {
                    1.0f,
                    1.0f
                };

                vertex->normal = {
                    normal[0],
                    normal[1],
                    normal[2]
                };

                if (uniqueVertices.count(vertex) == 0) {
                    uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                    vertices.push_back(*vertex);
                }

                indices.push_back(uniqueVertices[vertex]);
            }
        }
    }
    catch (std::exception & e) {
        std::cout << e.what() << std::endl;
    }

    model->geometryType = GeometryType::MODEL;
    model->vertices = vertices;
    model->indices = indices;
}

void ModelLoader::loadModelFromObjFile(std::string filePath, Model* model)
{
    std::vector<Vertex> vertices = {};
    std::vector<uint32_t> indices = {};

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filePath.c_str())) {
        throw std::runtime_error(warn + err);
    }

    std::unordered_map<Vertex*, uint32_t> uniqueVertices = {};

    for (const auto& shape : shapes) {

        std::vector<Vertex*> normalCalculVertices;

        for (const auto& index : shape.mesh.indices) {
            Vertex* vertex = new Vertex();

            vertex->pos = {
                attrib.vertices[3 * index.vertex_index + 0],
                attrib.vertices[3 * index.vertex_index + 1],
                attrib.vertices[3 * index.vertex_index + 2]
            };            

            vertex->texCoord = {
                attrib.texcoords[2 * index.texcoord_index + 0],
                1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
            };

            vertex->color = { 1.0f, 1.0f, 1.0f };

            if (uniqueVertices.count(vertex) == 0) {
                uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(*vertex);
            }

            normalCalculVertices.push_back(vertex);

            indices.push_back(uniqueVertices[vertex]);
        }

        glm::vec3 normal = glm::triangleNormal(normalCalculVertices[0]->pos, normalCalculVertices[1]->pos, normalCalculVertices[2]->pos);
        for (int i = 0; i < normalCalculVertices.size(); i++)
        {
            uniqueVertices[normalCalculVertices[i]];
        }
        normalCalculVertices = std::vector<Vertex*>();
    }

    model->geometryType = GeometryType::MODEL;
    model->vertices = vertices;
    model->indices = indices;
}

void ModelLoader::loadModelFromFile(std::string filePath, ModelFileType type, Model* model)
{
    switch (type)
    {
    case (ModelFileType::OBJ):
        loadModelFromObjFile(filePath, model);
        break;
    case (ModelFileType::STL):
        loadModelFromStlFile(filePath, model);
        break;
    case (ModelFileType::FBX):
        loadModelFromFbxFile(filePath, model);
        break;
    default:
        throw std::runtime_error("unsuported model file type !");
    }
}