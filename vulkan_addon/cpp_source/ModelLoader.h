#ifndef VKMASSIVE_MODEL_LOADER_H
#define VKMASSIVE_MODEL_LOADER_H

#include "Model.h"
#include <iostream>
#include <fstream>

#include "Vertex.h"
#include <tiny_obj_loader.h>
#include <stl_reader.h>
#include "OpenFBX/ofbx.h"

#include <unordered_map>
#include <vector>
#include <glm/gtx/normal.hpp>

enum ModelFileType {
	OBJ,
	STL,
	FBX
};

class ModelLoader
{
private:
	static void loadModelFromObjFile(std::string, Model*);
	static void loadModelFromStlFile(std::string, Model*);
	static void loadModelFromFbxFile(std::string, Model*);
public:
	static void loadModelFromFile(std::string filePath, ModelFileType type, Model* device);
};

#endif