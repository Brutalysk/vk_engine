#pragma once
#include <vector>
#include "UniformBuffer.h"
#include "Node.h"
#include "RtTriangle.h"
#include "Sphere.h"
#include "HittableList.h"
#include "BvhNode.h"

class ModelRenderInfo : public Hittable
{
public:
	std::vector<VkDescriptorPool> descriptorPools;
	std::vector<std::vector<VkDescriptorSet>> descriptorSets;
	std::vector<std::vector<UniformBuffer*>> uniformBuffers;
	std::shared_ptr<Node> nodeRef;
	std::shared_ptr<Hittable> rtModel;
	glm::mat4 calculatedTransformMatrix;


	ModelRenderInfo(std::shared_ptr<Node> node, Device* device, size_t viewPortCount, size_t swapChainImagesCount, glm::mat4 transformation)
	{
		this->nodeRef = node;
		this->descriptorSets = {};
		this->uniformBuffers = {};
		this->descriptorPools = {};
		this->calculatedTransformMatrix = transformation;

		createUniformBuffers(device, viewPortCount, swapChainImagesCount);
		setRtModel();
	}

	void createUniformBuffers(Device* device, size_t viewPortCount, size_t swapChainImagesCount)
	{
		uniformBuffers.resize(viewPortCount);
		for (size_t i = 0; i < viewPortCount; i++)
		{
			uniformBuffers[i].resize(swapChainImagesCount);
			for (size_t j = 0; j < swapChainImagesCount; j++)
			{
				uniformBuffers[i][j] = new UniformBuffer(device);
			}
		}
	}

	void setRtModel()
	{
		switch (this->nodeRef->model->geometryType)
		{
			case GeometryType::SPHERE:
				rtModel = std::make_shared<Sphere>(
					this->nodeRef->transformation.getTranslation(),
					this->nodeRef->transformation.getScale().x,
					this->nodeRef->material
				);
				break;
			default:
				HittableList model;
				std::vector<Vertex> vertices = this->nodeRef->model->vertices;
				std::vector<unsigned int> indices = this->nodeRef->model->indices;

				for (int i = 0; i < indices.size(); i += 3)
				{
					std::vector<Vertex> currentVertices = {};
					for(int j = 0; j < 3; j++)
						currentVertices.push_back(vertices[indices[i + j]]);

					model.add(make_shared<RtTriangle>(currentVertices, this->nodeRef->material, this->calculatedTransformMatrix));
					currentVertices.clear();
				}
				
				rtModel = make_shared<HittableList>(model);
				//TODO
				//rtModel = make_shared<BvhNode>(model);
				break;
		}
	}

	virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const
	{
		return rtModel->hit(r, tmin, tmax, rec);
	}

	virtual bool bounding_box(aabb& box) const
	{
		return rtModel->bounding_box(box);
	}
};

