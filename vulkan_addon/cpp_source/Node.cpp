#include "Node.h"
#include "ModelFactory.h"

std::shared_ptr<Node> Node::find(size_t id)
{
    if (this->id == id)
    {
        return std::shared_ptr<Node>(this);
    }
    else
    {
        std::vector<std::shared_ptr<Node>>::iterator childIte = this->childs.begin();
        while (childIte != this->childs.end())
        {
            std::shared_ptr<Node> result = static_cast<std::shared_ptr<Node>>(*childIte)->find(id);
            if (result != nullptr)
            {
                return result;
            }

            childIte++;
        }

        return nullptr;
    }
}

void Node::unlinkNode(json data)
{
    std::shared_ptr<Node> parent = this->find(data["parent"]);
    parent->removeChild(data["node"]);
}

void Node::linkNode(json data)
{
    std::shared_ptr<Node> parent = this->find(data["parent"]);
    std::shared_ptr<Node> child = this->find(data["node"]);
    parent->addChild(child);
}

void Node::addChild(std::shared_ptr<Node> child)
{
    this->childs.push_back(child);
    child->nbOfReferences ++;
}

void Node::removeChild(size_t id)
{
    bool childRemoved = false;

    std::vector<std::shared_ptr<Node>>::iterator childIte = this->childs.begin();
    while (childIte != this->childs.end() && !childRemoved)
    {
        if ((*childIte)->id == id)
        {
            (*childIte)->nbOfReferences --;
            this->childs.erase(childIte++);

            childRemoved = true;
        }
        else
        {
            childIte++;
        }
    }
}

json Node::jsonEncode(bool full)
{
    json nodeObject;
    nodeObject["id"] = id;
    nodeObject["name"] = name;

    if (material != nullptr)
    {
        nodeObject["material"] = material->jsonEncode();
    }

    if (model != nullptr)
    {
        nodeObject["model"] = model->jsonEncode();
    }

    nodeObject["transformation"] = transformation.jsonEncode();

    if (full)
    {
        for (int i = 0; i < childs.size(); i++)
        {
            nodeObject["childs"][i] = childs[i]->jsonEncode(full);
        }
    }

    return nodeObject;
}

void Node::updateFromJson(json data)
{
    this->name = data["name"];

    if(this->model != nullptr) {
        this->model->updateFromJson(data["model"]);
    }

    this->transformation.updateFromJson(data["transformation"]);
}