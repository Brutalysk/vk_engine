#ifndef VKMASSIVE_NODE_H
#define VKMASSIVE_NODE_H

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <nlohmann/json.hpp>

#include "Model.h"
#include "Vertex.h"
#include "ModelLoader.h"
#include "Transformation.h"
#include "GeometryType.h"
#include "Material.h"

#include "UuidFactory.h"

using json = nlohmann::json;

class ModelFactory;

class Node
{
public:
	size_t id;
	unsigned int nbOfReferences;
	std::string name;

	std::vector<std::shared_ptr<Node>> childs;

	Transformation transformation;
	Model* model;
	shared_ptr<Material> material;

	Node(size_t id) {
		this->id = id;
		this->transformation = Transformation();

		this->model = nullptr;
		this->material = nullptr;
	}

	Model generateScene();
	std::shared_ptr<Node> find(size_t id);
	void unlinkNode(json);
	void linkNode(json);
	void removeChild(size_t id);
	void addChild(std::shared_ptr<Node> child);
	json jsonEncode(bool full = true);
	void updateFromJson(json data);
};

#endif