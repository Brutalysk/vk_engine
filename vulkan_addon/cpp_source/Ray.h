#ifndef RAY_H
#define RAY_H

#include "Vertex.h"

class Ray
{
public:
	glm::vec3 origin;
	glm::vec3 direction;

	Ray() {}
	Ray(const glm::vec3& origin, const glm::vec3& direction) {
		this->origin = origin;
		this->direction = direction;
	}

	glm::vec3 at(double t) const {
		return origin + glm::vec3(t) * direction;
	}
};

#endif