#include "RtTriangle.h"


bool RtTriangle::hit(const Ray& r, double t0, double t1, hit_record& rec) const
{
    glm::vec3 mass_center = glm::vec3(0, 0, 10);
    glm::vec3 vorigin_masscenter = mass_center - r.origin;
    glm::vec3 proj = (glm::dot(vorigin_masscenter, r.direction) / glm::length2(r.direction)) * r.direction;
    glm::vec3 vto_center = vorigin_masscenter - proj;
    glm::vec3 norm_origin_direction = glm::normalize(r.direction);
    double mass = 2;
    double radius = glm::length(vto_center);
    double angle = 0;

    Utils::get_deflection_angle_around_mass(mass, radius, angle);

    if (angle > (2.4))
        return false;

    if (angle < 0.00000001)
        angle = 0;

    //Start calcul for curved part
    glm::vec3 deflected_vorigin_masscenter = vorigin_masscenter - vto_center + (norm_origin_direction * glm::vec3(radius));
    glm::vec3 deflected_v = vorigin_masscenter - deflected_vorigin_masscenter;

    glm::vec3 g = glm::cross(r.direction, glm::cross(deflected_v, r.direction));
    glm::vec3 g0 = g / glm::length(g) * glm::length(r.direction);
    glm::vec3 fprime = glm::vec3(cos(angle)) * deflected_vorigin_masscenter + glm::vec3(sin(angle)) * g0;

    Ray deflected_r;
    deflected_r.direction = fprime;
    deflected_r.origin = deflected_vorigin_masscenter;

    //deflected_r.origin = proj

    double slope = tan(angle / 2);
    glm::vec3 origin_v = glm::vec3(cos(angle / 2), sin(angle / 2), 0);
    glm::mat3 r_mat = glm::mat4(1);
    Utils::calculate_rot_between_vec(r.direction, origin_v, r_mat);

    glm::vec3 deflected_v_rotated = fprime * r_mat;


    // Intersection calcul
    glm::vec3 v0v1 = vertices[1].pos - vertices[0].pos;
    glm::vec3 v0v2 = vertices[2].pos - vertices[0].pos;

    glm::vec2 deltaUV1 = vertices[1].texCoord - vertices[0].texCoord;
    glm::vec2 deltaUV2 = vertices[2].texCoord - vertices[0].texCoord;

    //Hyperbole
    double a = mass * origin_v.x;
    double b = mass * origin_v.y;
    glm::vec3 const_vec = glm::cross(v0v1, v0v2);

    glm::vec3 pvec = glm::cross(deflected_r.direction, v0v2);
    float det = glm::dot(v0v1, pvec);

    // ray and triangle are parallel if det is close to 0
    if (fabs(det) < 0.001) return false;

    float invDet = 1 / det;

    glm::vec3 tvec = deflected_r.origin - vertices[0].pos;
    double u = glm::dot(tvec, pvec) * invDet;
    if (u < 0 || u > 1) return false;

    glm::vec3 qvec = glm::cross(tvec, v0v1);
    double v = glm::dot(deflected_r.direction, qvec) * invDet;
    if (v < 0 || u + v > 1) return false;

    double t = glm::dot(v0v2, qvec) * invDet;
    if (t < t0 || t > t1)
        return false;

    glm::vec2 P = vertices[0].texCoord + glm::mat2(u) * deltaUV1 + glm::mat2(v) * deltaUV2;
    rec.t = t;

    // Calcul de normal 
    //float ncalcr = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
    glm::vec3 tangent = (v0v1 * deltaUV2.y - v0v2 * deltaUV1.y);
    glm::vec3 bitangent = (v0v2 * deltaUV1.x - v0v1 * deltaUV2.x);
    glm::vec3 textureNormal = mp->getNormal(P.x, P.y, deflected_r.at(rec.t));

    glm::vec3 updatedNormal = (tangent * -textureNormal.x + bitangent * -textureNormal.y + normal * textureNormal.z);

    rec.u = P.x;
    rec.v = P.y;

    rec.set_face_normal(deflected_r, updatedNormal);

    rec.mat_ptr = mp;
    rec.p = deflected_r.at(rec.t);

    return true;
}

//bool RtTriangle::hit(const Ray& r, double t0, double t1, hit_record& rec) const 
//{
//    glm::vec3 v0v1 = vertices[1].pos - vertices[0].pos;
//    glm::vec3 v0v2 = vertices[2].pos - vertices[0].pos;
//
//    glm::vec2 deltaUV1 = vertices[1].texCoord - vertices[0].texCoord;
//    glm::vec2 deltaUV2 = vertices[2].texCoord - vertices[0].texCoord;
//
//    glm::vec3 pvec = glm::cross(r.direction, v0v2);
//    float det = glm::dot(v0v1, pvec);
//
//    // ray and triangle are parallel if det is close to 0
//    if (fabs(det) < 0.001) return false;
//
//    float invDet = 1 / det;
//
//    glm::vec3 tvec = r.origin - vertices[0].pos;
//    double u = glm::dot(tvec, pvec) * invDet;
//    if (u < 0 || u > 1) return false;
//
//    glm::vec3 qvec = glm::cross(tvec, v0v1);
//    double v = glm::dot(r.direction, qvec) * invDet;
//    if (v < 0 || u + v > 1) return false;
//    
//    double t = glm::dot(v0v2, qvec) * invDet;
//    if (t < t0 || t > t1)
//        return false;
//
//    glm::vec2 P = vertices[0].texCoord + glm::mat2(u) * deltaUV1 + glm::mat2(v) * deltaUV2;
//    rec.t = t;
//
//    // Calcul de normal 
//    //float ncalcr = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
//    glm::vec3 tangent = (v0v1 * deltaUV2.y - v0v2 * deltaUV1.y);
//    glm::vec3 bitangent = (v0v2 * deltaUV1.x - v0v1 * deltaUV2.x);
//    glm::vec3 textureNormal = mp->getNormal(P.x, P.y, r.at(rec.t));
//
//    glm::vec3 updatedNormal = (tangent * -textureNormal.x + bitangent * -textureNormal.y + normal * textureNormal.z);
//
//    rec.u = P.x;
//    rec.v = P.y;
//
//    rec.set_face_normal(r, updatedNormal);
//
//    rec.mat_ptr = mp;
//    rec.p = r.at(rec.t);
//
//    return true;
//}


bool RtTriangle::bounding_box(aabb& box) const 
{
    glm::vec3 min = vertices[0].pos;
    glm::vec3 max = vertices[0].pos;

    float diff = 0;

    for (int i = 1; i < vertices.size(); i++)
    {
        if (vertices[i].pos.x < min.x)
            min.x = vertices[i].pos.x;

        if (vertices[i].pos.y < min.y)
            min.y = vertices[i].pos.y;

        if (vertices[i].pos.z < min.z)
            min.z = vertices[i].pos.z;

        if (vertices[i].pos.x > max.x)
            max.x = vertices[i].pos.x;

        if (vertices[i].pos.y > max.y)
            max.y = vertices[i].pos.y;

        if (vertices[i].pos.z > max.z)
            max.z = vertices[i].pos.z;
    }

    for (int i = 0; i < 3; i++) {
        if ((max[i] - min[i]) > diff)
            diff = (max[i] - min[i]);
    }

    for (int i = 0; i < 3; i++)
    {
        if ((max[i] - min[i]) == 0)
        {
            max[i] += diff/1000;
            min[i] -= diff/1000;
        }
    }

    box = aabb(min, max);

    return true;
}