#pragma once
#include "Hittable.h"
#include <glm/gtx/normal.hpp>
#include "Transformation.h"
#include "Material.h"

class RtTriangle : public Hittable
{
public:
	shared_ptr<Material> mp;
	std::vector<Vertex> vertices;
	glm::vec3 normal;

	RtTriangle() {}

	RtTriangle(std::vector<Vertex> vertices, shared_ptr<Material> mat, glm::mat4 transformation) {
		Transformation decomposedTransform = Transformation(transformation);

		this->mp = mat;
		this->normal = glm::vec3(-1) * glm::triangleNormal(vertices[0].pos, vertices[1].pos, vertices[2].pos) * decomposedTransform.getRotationMatrix();
		
		for(size_t i = 0; i < 3; i++)
		{
			glm::vec4 tmp(vertices[i].pos, 1.0f);
			tmp = tmp * transformation;

			for(size_t j = 0; j < 3; j++)
				vertices[i].pos[j] = (tmp[j] + transformation[3][j]);
		}
		
		this->vertices = vertices;
	}

	virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
	virtual bool bounding_box(aabb& box) const;
};

