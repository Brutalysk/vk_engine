#include "SceneGraph.h"

void SceneGraph::addNode(size_t parentId, std::shared_ptr<Node> node)
{
	this->nodes[parentId]->addChild(node);
	if (nodes.count(node->id) == 0)
	{
		nodes[node->id] = node;
	}
}

void SceneGraph::generateModelRenderInfos(Device* device, size_t viewPortCount, size_t swapChainImagesCount)
{
	world = new HittableList();
	modelRenderInfos = {};
	generateModelRenderInfosRec(this->getBase(), device, viewPortCount, swapChainImagesCount, glm::mat4(1.0f));
}

void SceneGraph::generateModelRenderInfosRec(std::shared_ptr<Node> node, Device* device, size_t viewPortCount, size_t swapChainImagesCount, glm::mat4 parentTransforms)
{
	glm::mat4 transformMatrix = parentTransforms * node->transformation.getTransformationMatrix();

	if (node->material != nullptr && node->model != nullptr)
	{
		shared_ptr<ModelRenderInfo> info = make_shared<ModelRenderInfo>(node, device, viewPortCount, swapChainImagesCount, transformMatrix);
		modelRenderInfos.push_back(info);
		world->add(info);
	}

	for (size_t i = 0; i < node->childs.size(); i++)
	{
		std::cout << node->childs[i]->name << std::endl;
		generateModelRenderInfosRec(node->childs[i], device, viewPortCount, swapChainImagesCount, transformMatrix);
	}
}

void SceneGraph::generateBuffers(Device* device)
{
	std::vector<Vertex> vertices = {};
	std::vector<uint32_t> indices = {};

	std::unordered_map<size_t, std::shared_ptr<Node>>::iterator it = nodes.begin();

	size_t offset = 0;
	while (it != nodes.end())
	{
		if (it->second->material != nullptr)
		{
			it->second->model->vertexOffset = offset;
			offset += it->second->model->vertices.size();

			vertices.insert(vertices.end(), it->second->model->vertices.begin(), it->second->model->vertices.end());
			indices.insert(indices.end(), it->second->model->indices.begin(), it->second->model->indices.end());
		}

		it++;
	}

	vertexBuffer = new VertexBuffer(device, vertices);
	indexBuffer = new IndexBuffer(device, indices);
}

std::shared_ptr<Node> SceneGraph::getNode(size_t nodeId)
{
	return this->nodes[nodeId];
}

std::shared_ptr<Node> SceneGraph::getBase()
{
	return this->base;
}

void SceneGraph::linkNode(size_t parentId, size_t nodeId)
{
	nodes[parentId]->addChild(nodes[nodeId]);
}

void SceneGraph::unlinkNode(size_t parentId, size_t nodeId)
{
	nodes[parentId]->removeChild(nodeId);
}

void SceneGraph::cleanupBuffers() 
{
	for (size_t i = 0; i < modelRenderInfos.size(); i++)
	{
		for (size_t j = 0; j < modelRenderInfos[i]->descriptorPools.size(); j++)
		{
			vkDestroyDescriptorPool(device->vkDevice, modelRenderInfos[i]->descriptorPools[j], nullptr);
		}
	}

	vkDestroyBuffer(device->vkDevice, indexBuffer->indexBuffer, nullptr);
	vkFreeMemory(device->vkDevice, indexBuffer->indexBufferMemory, nullptr);

	vkDestroyBuffer(device->vkDevice, vertexBuffer->vertexBuffer, nullptr);
	vkFreeMemory(device->vkDevice, vertexBuffer->vertexBufferMemory, nullptr);
}