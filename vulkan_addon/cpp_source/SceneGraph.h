#pragma once
#include "Node.h"
#include "ModelRenderInfo.h"
#include "HittableList.h"

class SceneGraph
{
public:
	HittableList* world;
	std::unordered_map<size_t, std::shared_ptr<Node>> nodes;
	std::vector<shared_ptr<ModelRenderInfo>> modelRenderInfos;
	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	void addNode(size_t parentId, std::shared_ptr<Node>);
	std::shared_ptr<Node> getNode(size_t nodeId);
	std::shared_ptr<Node> getBase();
	void linkNode(size_t parentId, size_t nodeId);
	void unlinkNode(size_t parentId, size_t nodeId);

	SceneGraph(Device* device, std::shared_ptr<Node> base) {
		this->base = base;
		nodes[base->id] = base;
		world = new HittableList();
	}

	void generateModelRenderInfos(Device* device, size_t viewPortCount, size_t swapChainImagesCount);
	void generateBuffers(Device*);

	void cleanupBuffers();
private:
	void generateModelRenderInfosRec(std::shared_ptr<Node> node, Device* device, size_t viewPortCount, size_t swapChainImagesCount, glm::mat4);
	std::shared_ptr<Node> base;
	Device* device;
};