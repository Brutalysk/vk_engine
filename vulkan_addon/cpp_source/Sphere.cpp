#include "Sphere.h"

bool Sphere::hit(const Ray& r, double t_min, double t_max, hit_record& rec) const {
    glm::vec3 oc = r.origin - center;
    auto a = glm::dot(r.direction, r.direction);
    auto half_b = glm::dot(oc, r.direction);
    auto c = glm::dot(oc, oc) - radius * radius;
    auto discriminant = half_b * half_b - a * c;

    if (discriminant > 0) {
        auto root = sqrt(discriminant);
        auto temp = (-half_b - root) / a;

        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.at(rec.t);
            glm::vec3 outward_normal = (rec.p - center) / glm::vec3(radius);
            Utils::get_sphere_uv(glm::vec3(rec.p - center) / glm::vec3(radius), rec.u, rec.v);

            // Update normal
            glm::vec3 A = glm::vec3({ 0, 0, 1 }) * glm::vec3(this->radius);
            glm::vec3 tangent = glm::normalize(glm::cross(A, (r.at(rec.t)) - this->center));
            glm::vec3 bitangent = glm::cross(outward_normal, tangent);
            glm::vec3 textureNormal = mat_ptr->getNormal(rec.u, rec.v, r.at(rec.t));
            glm::vec3 updatedNormal = (tangent * -textureNormal.x + bitangent * -textureNormal.y + outward_normal * textureNormal.z);

            rec.set_face_normal(r, updatedNormal);
            rec.mat_ptr = mat_ptr;
            return true;
        }
        temp = (-half_b + root) / a;
        if (temp < t_max && temp > t_min) {
            rec.t = temp;
            rec.p = r.at(rec.t);
            glm::vec3 outward_normal = (rec.p - center) / glm::vec3(radius);
            Utils::get_sphere_uv(glm::vec3(rec.p - center) / glm::vec3(radius), rec.u, rec.v);

            // Update normal
            glm::vec3 A = glm::vec3({ 0, 0, 1 }) * glm::vec3(this->radius);
            glm::vec3 tangent = glm::normalize(glm::cross(A, (r.at(rec.t)) - this->center));
            glm::vec3 bitangent = glm::cross(outward_normal, tangent);
            glm::vec3 textureNormal = mat_ptr->getNormal(rec.u, rec.v, r.at(rec.t));
            glm::vec3 updatedNormal = (tangent * -textureNormal.x + bitangent * -textureNormal.y + outward_normal * textureNormal.z);

            rec.set_face_normal(r, updatedNormal);
            rec.mat_ptr = mat_ptr;
            return true;
        }
    }
    return false;
}

bool Sphere::bounding_box(aabb& box) const
{
    box = aabb(
        center - glm::vec3(radius), 
        center + glm::vec3(radius));
    return true;
}


aabb surrounding_box(aabb box0, aabb box1) {
    glm::vec3 small(fmin(box0.min.x, box1.min.x),
        fmin(box0.min.y, box1.min.y),
        fmin(box0.min.z, box1.min.z));

    glm::vec3 big(fmax(box0.max.x, box1.max.x),
        fmax(box0.max.y, box1.max.y),
        fmax(box0.max.z, box1.max.z));

    return aabb(small, big);
}
