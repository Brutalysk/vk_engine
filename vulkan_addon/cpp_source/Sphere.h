#ifndef SPHERE_H
#define SPHERE_H

#include "Hittable.h"
#include "Material.h"

class Sphere : public Hittable
{
public:
	glm::vec3 center;
	double radius;
	std::shared_ptr<Material> mat_ptr;

	Sphere() {}
	Sphere(glm::vec3 cen, double r, std::shared_ptr<Material> m)
		: center(cen), radius(r), mat_ptr(m) {};

	virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
	virtual bool bounding_box(aabb& box) const;
};

#endif