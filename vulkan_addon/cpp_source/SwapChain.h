#ifndef VKMASSIVE_SWAP_CHAIN_H
#define VKMASSIVE_SWAP_CHAIN_H

#include <vulkan/vulkan.h>
#include <vector>
#include <algorithm>
#include <glfw3.h>

#include "Device.h"
#include "ImageService.h"
#include "ViewPort.h"

class ImageService;
class ViewPort;

class SwapChain
{
public:
	VkSwapchainKHR swapChain;
	std::vector<VkImage> swapChainImages;
	VkFormat swapChainImageFormat;
	VkExtent2D swapChainExtent;
	std::vector<VkImageView> swapChainImageView;
	std::vector<VkFramebuffer> swapChainFramebuffers;

	SwapChain(Device* device, ViewPort* viewPort, ImageService* imageService) {
		this->device = device;
		this->viewPort = viewPort;
		this->imageService = imageService;

		createSwapChain();
		createImageViews();
	}

	~SwapChain() {
		for (auto imageView : this->swapChainImageView) {
			vkDestroyImageView(device->vkDevice, imageView, nullptr);
		}

		vkDestroySwapchainKHR(device->vkDevice, this->swapChain, nullptr);
	}

private:
	Device* device;
	ViewPort* viewPort;
	ImageService* imageService;

	void createSwapChain();
	void createImageViews();

	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);
	SwapChainSupportDetails querySwapChainSupport();
	VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
};

#endif