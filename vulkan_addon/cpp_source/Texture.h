#ifndef VKMASSIVE_TEXTURE_H
#define VKMASSIVE_TEXTURE_H

#include <vulkan/vulkan.h>
#include <vector>
#include <glm/glm.hpp>

class Texture
{
public:
    VkDescriptorSet textureDescriptorSet;

    VkImage textureImage;
    VkDeviceMemory textureImageMemory;
    VkImageView textureImageView;
    VkSampler textureSampler;

	virtual glm::vec3 value(double u, double v, const glm::vec3& p) const = 0;
};

#endif