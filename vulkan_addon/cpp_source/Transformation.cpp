#include "Transformation.h"

glm::vec3 Transformation::getTranslation() {
    return this->translation;
}

glm::vec3 Transformation::getRotation() {
    return this->rotation;
}

glm::vec3 Transformation::getScale() {
    return this->scale;
}

glm::mat3 Transformation::getRotationMatrix() {
    glm::mat3 rotation =
        glm::rotate(glm::mat4(1.0f), this->rotation.x, glm::vec3({ 1.0f, 0.0f, 0.0f })) *
        glm::rotate(glm::mat4(1.0f), this->rotation.y, glm::vec3({ 0.0f, 1.0f, 0.0f })) *
        glm::rotate(glm::mat4(1.0f), this->rotation.z, glm::vec3({ 0.0f, 0.0f, 1.0f }));

    return rotation;
}

glm::mat4 Transformation::getTransformationMatrix() {
    return this->transformationMatrix;
}

void Transformation::setTranslation(glm::vec3 translation)
{
    this->translation = translation;
    this->calculateTransformationMatrix();
}

void Transformation::setRotation(glm::vec3 rotation)
{
    this->rotation = rotation;
    this->calculateTransformationMatrix();
}

void Transformation::setScale(glm::vec3 scale)
{
    this->scale = scale;
    this->calculateTransformationMatrix();
}

json Transformation::jsonEncode()
{
    json data;

    for (int i = 0; i < 3; i++)
    {
        data["translation"][i] = this->translation[i];
        data["rotation"][i] = this->rotation[i];
        data["scale"][i] = this->scale[i];
    }

    return data;
}

void Transformation::updateFromJson(json data)
{
    for (int i = 0; i < 3; i++)
    {
        this->translation[i] = data["translation"][i];
        this->rotation[i] = data["rotation"][i];
        this->scale[i] = data["scale"][i];
    }

    this->calculateTransformationMatrix();
}

void Transformation::calculateTransformationMatrix() {
    glm::mat4 rotation = this->getRotationMatrix();

    this->transformationMatrix =  rotation * glm::translate(glm::mat4(1.0f), this->translation) * glm::scale(glm::mat4(1.0f), this->scale);
}

