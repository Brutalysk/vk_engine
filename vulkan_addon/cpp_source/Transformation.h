#ifndef VKMASSIVE_TRANSFORMATION_H
#define VKMASSIVE_TRANSFORMATION_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <nlohmann/json.hpp>
#include <glm/gtx/norm.hpp>

using json = nlohmann::json;

class Transformation
{
private:
	glm::vec3 translation;
	glm::vec3 rotation;
	glm::vec3 scale;
	glm::mat4 transformationMatrix;

	void calculateTransformationMatrix();

public:
	glm::vec3 getTranslation();
	glm::vec3 getRotation();
	glm::vec3 getScale();

	glm::mat3 getRotationMatrix();
	glm::mat4 getTransformationMatrix();

	void setTranslation(glm::vec3);
	void setRotation(glm::vec3);
	void setScale(glm::vec3);

	json jsonEncode();
	void updateFromJson(json);

	Transformation() {
		this->translation = { 0, 0, 0 };
		this->rotation = { 0, 0, 0 };
		this->scale = { 1, 1, 1 };

		this->calculateTransformationMatrix();
	}

	Transformation(glm::mat4 transformMatrix) {
		this->transformationMatrix = transformMatrix;

		this->translation = { transformMatrix[3].x, transformMatrix[3].y, transformMatrix[3].z };
		transformMatrix[3] = { 0, 0, 0, 1 };

		this->scale = {
			glm::l2Norm(glm::vec3{
				transformMatrix[0].x,
				transformMatrix[0].y,
				transformMatrix[0].z,
			}),
			glm::l2Norm(glm::vec3{
				transformMatrix[1].x,
				transformMatrix[1].y,
				transformMatrix[1].z,
			}),
			glm::l2Norm(glm::vec3{
				transformMatrix[2].x,
				transformMatrix[2].y,
				transformMatrix[2].z,
			})
		};
		transformMatrix[0] = transformMatrix[0] / this->scale.x;
		transformMatrix[1] = transformMatrix[1] / this->scale.y;
		transformMatrix[2] = transformMatrix[2] / this->scale.z;

		auto det = glm::determinant(transformationMatrix);
		if (glm::determinant(transformationMatrix) < 0)
		{
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					transformMatrix[i][j] *= -1;
				}
			}
		}

		this->rotation = {
			atan2(-transformMatrix[0].z, transformMatrix[0].x),
			asin(transformMatrix[0].y),
			atan2(-transformMatrix[2].y, transformMatrix[1].y),

		};

	}
};

#endif