#pragma once
#include <vector>
#include <vulkan/vulkan.h>
#include "UniformBufferObject.h"
#include "Buffer.h"

class UniformBuffer : Buffer
{
public:
    VkBuffer uniformBuffer;
    VkDeviceMemory uniformBufferMemory;

    UniformBuffer(Device* device) 
    : Buffer(device)
    {
        VkDeviceSize bufferSize = sizeof(UniformBufferObject);
        createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffer, uniformBufferMemory);
    }

    ~UniformBuffer() {
        vkDestroyBuffer(device->vkDevice, uniformBuffer, nullptr);
        vkFreeMemory(device->vkDevice, uniformBufferMemory, nullptr);
    }
};

