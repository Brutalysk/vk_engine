#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <limits>
#include <memory>
#include <functional>
#include <random>

// Usings

using std::shared_ptr;
using std::make_shared;

// Constants

const double infinity = std::numeric_limits<double>::infinity();
const double pi = 3.1415926535897932385;
const int c = 299792458;	// in m/s
const int c_2 = 89875518000000000;	// in m/s
const double g = 0.000000000066743;

// Adjusted values not to bust double limit, use only when calculating angle diff
const double g_over_c2_g = 7.42616;		// (g / c ^ 2) * 10 ^ 28
const double g_solm = 200;				// M		   / 10 ^ 28  (M = One solar mass)


// Common Headers

#include "Ray.h"
#include <glm/glm.hpp>

class Utils
{
public:
	static double clamp(double x, double min, double max) {
		if (x < min) return min;
		if (x > max) return max;
		return x;
	}

	static void write_color(glm::vec3 color, std::ostream& out, int samples_per_pixel) {
		// Divide the color total by the number of samples and gamma-correct
		// for a gamma value of 2.0.
		auto scale = 1.0 / samples_per_pixel;
		auto r = sqrt(scale * color[0]);
		auto g = sqrt(scale * color[1]);
		auto b = sqrt(scale * color[2]);

		// Write the translated [0,255] value of each color component.
		out << static_cast<int>(256 * clamp(r, 0.0, 0.999)) << ' '
			<< static_cast<int>(256 * clamp(g, 0.0, 0.999)) << ' '
			<< static_cast<int>(256 * clamp(b, 0.0, 0.999)) << '\n';
	}

	static double schlick(double cosine, double ref_idx) {
		auto r0 = (1 - ref_idx) / (1 + ref_idx);
		r0 = r0 * r0;
		return r0 + (1 - r0) * pow((1 - cosine), 5);
	}

	static double degrees_to_radians(double degrees) {
		return degrees * pi / 180;
	}

	static glm::vec3 random_vec3() {
		return glm::vec3(random_double(), random_double(), random_double());
	}

	static glm::vec3 random_vec3(double min, double max) {
		return glm::vec3(random_double(min, max), random_double(min, max), random_double(min, max));
	}

	static glm::vec3 random_unit_vector() {
		auto a = random_double(0, 2 * pi);
		auto z = random_double(-1, 1);
		auto r = sqrt(1 - z * z);
		return glm::vec3(r * cos(a), r * sin(a), z);
	}

	static glm::vec3 random_in_unit_sphere() {
		while (true) {
			auto p = random_vec3(-1, 1);
			if (glm::dot(p, p) >= 1) continue;
			return p;
		}
	}
	
	static glm::vec3 random_in_hemisphere(const glm::vec3& normal) {
		glm::vec3 in_unit_sphere = random_in_unit_sphere();
		if (glm::dot(in_unit_sphere, normal) > 0.0) // In the same hemisphere as the normal
			return in_unit_sphere;
		else
			return -in_unit_sphere;
	}

	static int random_int(int min, int max) {
		int random_integer;
		int range = (max - min) + 1;

		return range * (rand() / (RAND_MAX + 1.0));
	}

	static double random_double() {
		static std::uniform_real_distribution<double> distribution(0.0, 1.0);
		static std::mt19937 generator;
		static std::function<double()> rand_generator =
			std::bind(distribution, generator);
		return rand_generator();
	}

	static double random_double(double min, double max) {
		// Returns a random real in [min,max).
		return min + (max - min) * random_double();
	}

	static void get_sphere_uv(const glm::vec3& p, double& u, double& v) {
		auto phi = atan2(p.z, p.x);
		auto theta = asin(p.y);
		u = 1 - (phi + pi) / (2 * pi);
		v = (theta + pi / 2) / pi;
	}

	static void get_deflection_angle_around_mass(double& m, double& r, double& theta) {
		theta = (m / r) * g_over_c2_g;
	}

	static void calculate_rot_between_vec(const glm::vec3& a, const glm::vec3& b, glm::mat3& r)
	{
		glm::vec3 v = glm::cross(b, a);
		float angle = acos(glm::dot(b, a) / (glm::length(b) * glm::length(a)));
		r = glm::rotate(angle, v);
	}

	static double ffmin(double a, double b) { return a <= b ? a : b; }
	static double ffmax(double a, double b) { return a >= b ? a : b; }
};

