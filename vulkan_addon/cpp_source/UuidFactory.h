#ifndef VKMASSIVE_UUID_FACTORY_H
#define VKMASSIVE_UUID_FACTORY_H

class UuidFactory
{
public:
    static int currentId;
    static int getUid();
};

#endif