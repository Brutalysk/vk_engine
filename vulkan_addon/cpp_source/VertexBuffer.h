#pragma once
#include "Vertex.h"
#include "Buffer.h"

class VertexBuffer : Buffer
{
public:
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;

    VertexBuffer(Device* device, std::vector<Vertex> vertices)
    :Buffer(device) 
    {
        VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

        createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

        void* data;
        vkMapMemory(device->vkDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
        memcpy(data, vertices.data(), (size_t)bufferSize);
        vkUnmapMemory(device->vkDevice, stagingBufferMemory);

        createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);

        copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

        vkDestroyBuffer(device->vkDevice, stagingBuffer, nullptr);
        vkFreeMemory(device->vkDevice, stagingBufferMemory, nullptr);
    }

    ~VertexBuffer()
    {
        vkDestroyBuffer(device->vkDevice, vertexBuffer, nullptr);
        vkFreeMemory(device->vkDevice, vertexBufferMemory, nullptr);
    }
};

