#include "ViewPort.h"

void ViewPort::createSurface()
{
    if (glfwCreateWindowSurface(instance, glfwWindow , nullptr, &this->surface) != VK_SUCCESS) {
        throw std::runtime_error("failed to create window surface!");
    }
}

void ViewPort::initWindow() {
    glfwWindow = glfwCreateWindow(width, height, "vkMassive", nullptr, nullptr);
    glfwSetWindowUserPointer(glfwWindow, this);
    glfwSetFramebufferSizeCallback(glfwWindow, framebufferResizeCallback);
}

void ViewPort::framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    auto app = reinterpret_cast<ViewPort*>(glfwGetWindowUserPointer(window));
    app->framebufferResized = true;
}