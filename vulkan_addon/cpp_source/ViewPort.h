#pragma once
#include <vector>
#include <vulkan/vulkan.h>
#include <glfw3.h>
#include <algorithm>
#include "Device.h"
#include "ImageService.h"

class ViewPort
{
public:
	GLFWwindow* glfwWindow;
	VkSurfaceKHR surface;

	bool framebufferResized;

	ViewPort(VkInstance instance, size_t width, size_t height) {
		this->width = width;
		this->height = height;

		this->instance = instance;

		framebufferResized = false;

		initWindow();
		createSurface();
	}

	~ViewPort() {
		glfwDestroyWindow(glfwWindow);
	}

private:
	size_t width;
	size_t height;

	VkInstance instance;

	static void framebufferResizeCallback(GLFWwindow* window, int width, int height);
	void createSurface();
	void initWindow();
};

