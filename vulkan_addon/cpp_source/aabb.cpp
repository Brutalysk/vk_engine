#include "aabb.h"

bool aabb::hit(const Ray& r, float tmin, float tmax) const {
    for (int a = 0; a < 3; a++) {
        auto invD = 1.0f / r.direction[a];
        auto t0 = (min[a] - r.origin[a]) * invD;
        auto t1 = (max[a] - r.origin[a]) * invD;
        if (invD < 0.0f)
            std::swap(t0, t1);
        tmin = t0 > tmin ? t0 : tmin;
        tmax = t1 < tmax ? t1 : tmax;
        if (tmax <= tmin)
            return false;
    }
    return true;
}

aabb aabb::surrounding_box(aabb box0, aabb box1) {
    glm::vec3 small(
        Utils::ffmin(box0.min.x, box1.min.x),
        Utils::ffmin(box0.min.y, box1.min.y),
        Utils::ffmin(box0.min.z, box1.min.z));
    glm::vec3 big(
        Utils::ffmax(box0.max.x, box1.max.x),
        Utils::ffmax(box0.max.y, box1.max.y),
        Utils::ffmax(box0.max.z, box1.max.z));

    return aabb(small, big);
}