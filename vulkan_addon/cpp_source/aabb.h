#pragma once
#include <glm\glm.hpp>
#include "Ray.h"
#include "Utils.h"

class aabb
{
public:
	glm::vec3 min;
	glm::vec3 max;

	aabb(){}
	aabb(const glm::vec3& a, const glm::vec3& b) { min = a; max = b; }

	bool hit(const Ray& r, float tmin, float tmax) const;
	static aabb surrounding_box(aabb box0, aabb box1);
};

