#pragma once
#include "HittableList.h"
#include "xy_rect.h"
#include "xz_rect.h"
#include "yz_rect.h"

class box : public Hittable
{
public:
    box() {}
    box(const glm::vec3& p0, const glm::vec3& p1, shared_ptr<Material> ptr);

    virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
    virtual bool bounding_box(aabb& box) const;

    glm::vec3 box_min;
    glm::vec3 box_max;
    HittableList sides;
};

