#ifdef NODE_API
#include <nan.h>
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <windows.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#define TINYOBJLOADER_IMPLEMENTATION
#define TINYGLTF_LOADER_IMPLEMENTATION

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <chrono>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <cstdint>
#include <array>
#include <optional>
#include <set>
#include <queue>

#include "Utils.h"
#include "Vertex.h"
#include "ModelFactory.h"
#include "ModelLoader.h"
#include "Node.h"
#include "Model.h"
#include "Camera.h"
#include "Device.h"
#include "ViewPort.h"
#include "SwapChain.h"
#include "DepthImage.h"
#include "UniformBufferObject.h"
#include "DescriptorService.h"
#include "UniformBuffer.h"
#include "SceneGraph.h"

#include "UuidFactory.h"
#include "Hittable.h"
#include "HittableList.h"
#include "Sphere.h"
#include "Lambertian.h"
#include "Metal.h"
#include "Dielectric.h"
#include "ConstantTexture.h"
#include "CheckerTexture.h"
#include "xy_rect.h"
#include "DiffuseLight.h"
#include "RtTriangle.h"
#include "yz_rect.h"
#include "xz_rect.h"
#include "box.h"
int UuidFactory::currentId = 1;

bool ready = false;

const int WIDTH = 800;
const int HEIGHT = 800;

const int MAIN_WINDOW_ID = 0;
const int WINDOW_COUNT = 1;

const int MAX_FRAMES_IN_FLIGHT = 2;

const char *WHITE_TEXTURE_PATH = "resources/textures/white.jpg";
const char *PURPLE_CHECKERBOARD_TEXTURE_PATH = "resources/textures/default.png";

//Multithreading section
typedef struct MyData
{
    HittableList world;
    int image_width;
    int image_height;
    int samples_per_pixel;
    int max_depth;
    glm::vec3 background;

    std::vector<glm::vec3> results;
} MYDATA, *PMYDATA;

#define MAX_THREADS 42
#define BUF_SIZE 255
HANDLE ghMutex; 

//Multithreading section end

PMYDATA pDataArray[MAX_THREADS];
DWORD dwThreadIdArray[MAX_THREADS];
HANDLE hThreadArray[MAX_THREADS];

const std::vector<const char *> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME};

const bool enableValidationLayers = false;

enum ActionTypes
{
    UPDATE_NODE,
    CREATE_NODE
};

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo, const VkAllocationCallbacks *pAllocator, VkDebugUtilsMessengerEXT *pDebugMessenger)
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr)
    {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks *pAllocator)
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr)
    {
        func(instance, debugMessenger, pAllocator);
    }
}

typedef struct render_thread_data
{
    HittableList *world;
    int image_width;
    int image_height;
    int samples_per_pixel;
    int max_depth;
    glm::vec3 background;
    Camera *camera;
    int thread_id;

    std::vector<glm::vec3> results;
};

glm::vec3 ray_color(const Ray &r, const glm::vec3 &background, const Hittable &world, int depth)
{
    hit_record rec;

    // If we've exceeded the ray bounce limit, no more light is gathered.
    if (depth <= 0)
        return glm::vec3(0, 0, 0);

    // If the ray hits nothing, return the background color.
    if (!world.hit(r, 0.001, infinity, rec))
        return background;

    Ray scattered;
    glm::vec3 attenuation;
    glm::vec3 emitted = rec.mat_ptr->emitted(rec.u, rec.v, rec.p);
    if (!rec.mat_ptr->scatter(r, rec, attenuation, scattered))
        return emitted;

    return emitted + attenuation * ray_color(scattered, background, world, depth - 1);
}

DWORD WINAPI renderThread(LPVOID lpParameter)
{
    render_thread_data *td = (render_thread_data *)lpParameter;

    for (int j = td->image_height - 1; j >= 0; --j)
    {
        if (td->thread_id == 0)
        {
            std::cerr << "\rScanlines remaining: " << j << ' ' << std::flush;
        }
        for (int i = 0; i < td->image_width; ++i)
        {
            glm::vec3 color(0, 0, 0);
            for (int s = 0; s < td->samples_per_pixel; ++s)
            {
                auto u = (i + Utils::random_double()) / td->image_width;
                auto v = (j + Utils::random_double()) / td->image_height;
                Ray r = td->camera->getRay(u, v);
                color += ray_color(r, td->background, *td->world, td->max_depth);
            }

            td->results.push_back(color);
        }
    }
    if (td->thread_id == 0)
    {
        std::cerr << "\nDone.\n";
    }
    else
    {
        std::cout << "Thread " << td->thread_id << " is done." << std::endl;
    }

    return 0;
}

Camera *mainCamera;
const glm::vec3 vup = {0, 1, 0};

class VkMassive
{
public:
    std::shared_ptr<SceneGraph> sceneGraph;

    void unlinkNode(json data)
    {
        renderPaused = true;

        while (frameInProgress)
        {
            Sleep(50);
        }

        sceneGraph->unlinkNode(data["parent"], data["node"]);

        framebufferResized = true;
        renderPaused = false;
    }

    void linkNode(json data)
    {
        renderPaused = true;

        while (frameInProgress)
        {
            Sleep(50);
        }

        sceneGraph->linkNode(data["parent"], data["node"]);

        framebufferResized = true;
        renderPaused = false;
    }

    void startRender(json data)
    {
        renderPaused = true;
        while (frameInProgress)
        {
            Sleep(50);
        }
        this->renderRTImage(data);
        renderPaused = false;
    }

    void updateCamera(json data)
    {
        renderPaused = true;

        while (frameInProgress)
        {
            Sleep(50);
        }

        mainCamera->updateFromJson(data);

        renderPaused = false;
    }

    void updateNode(json data)
    {
        renderPaused = true;

        while (frameInProgress)
        {
            Sleep(50);
        }

        std::shared_ptr<Node> node = sceneGraph->nodes[data["id"]];

        if (data["model"] != nullptr && data["model"]["geometryType"] != -1)
        {
            if (data["model"]["geometryPath"] != nullptr)
            {
                ModelLoader::loadModelFromFile(data["model"]["geometryPath"], data["model"]["fileType"], node->model);
            }
            else
            {
                if (node->model == nullptr || data["model"]["geometryType"] != node->model->geometryType)
                {
                    Model *model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
                    ModelFactory::getShape(data["model"]["geometryType"], model);
                    node->model = model;
                }
            }
        }
        else
        {
            if (node->model != nullptr)
            {
                node->model = nullptr;
            }
        }

        node->updateFromJson(data);
        framebufferResized = true;
        renderPaused = false;
    }

    void updateNodeMaterial(json data)
    {
        renderPaused = true;

        while (frameInProgress)
        {
            Sleep(50);
        }

        std::shared_ptr<Node> node = sceneGraph->nodes[data["nodeId"]];
        
        glm::vec3 color(1);

        if (data["color"] != nullptr)
            for (size_t i = 0; i < 3; i++)
                color[i] = data["color"][i];

        std::shared_ptr<Texture> texture;

        if (data["texture"] == nullptr) {
            if(node->material != nullptr && node->material->texture != nullptr)
                texture = node->material->texture;
            else
                texture = imageService->createTexture(WHITE_TEXTURE_PATH);
        }
        else
        {
            std::string texturePath = data["texture"];
            texture = imageService->createTexture(texturePath.c_str());
        }

        switch ((MaterialType)data["materialType"])
        {
        case (MaterialType::LAMBERTIAN):
        {
            node->material = make_shared<Lambertian>(color, texture);
        }
        break;
        case (MaterialType::DIFF_LIGHT):
        {
            if (data["intensity"] != nullptr)
                color = color * glm::vec3(data["intensity"]);

            node->material = make_shared<DiffuseLight>(color, texture);
        }
        break;
        case (MaterialType::DIELECTRIC):
        {
            double refIndex = 1.5;
            if (data["refIndex"] != nullptr)
                refIndex = data["refIndex"];

            node->material = make_shared<Dielectric>(color, refIndex, texture);
        }
        break;
        case (MaterialType::METALIC):
        {
            double fuzz = 0.0f;

            if (data["fuzz"] != nullptr)
                fuzz = data["fuzz"];

            node->material = make_shared<Metal>(color, fuzz, texture);
        }
        break;
        case (MaterialType::CHECKERBOARD):
        {
            std::shared_ptr<CheckerTexture> checkerTexture = make_shared<CheckerTexture>(
                make_shared<ConstantTexture>(color * glm::vec3(0.9)),
                make_shared<ConstantTexture>(color * glm::vec3(0.1)));

            checkerTexture->textureDescriptorSet = texture->textureDescriptorSet;
            checkerTexture->textureImage = texture->textureImage;
            checkerTexture->textureImageMemory = texture->textureImageMemory;
            checkerTexture->textureImageView = texture->textureImageView;
            checkerTexture->textureSampler = texture->textureSampler;

            node->material = make_shared<Lambertian>(color, checkerTexture);
        }
        break;
        default:
        {
            throw std::runtime_error("invalid material type requested!");
        }
        break;
        }

        //Update the normal map
        if (data["normalMap"] != nullptr) {
            std::string normalMapPath = data["normalMap"];
            node->material->normalMap = imageService->createTexture(normalMapPath.c_str());
        }

        framebufferResized = true;
        renderPaused = false;
    }

    json getNodeMaterial(int nodeId)
    {
        return sceneGraph->nodes[nodeId]->material->jsonEncode();
    }

    void run()
    {
        initWindow();
        initVulkan();

        json data;
        data["samplesPerPixel"] = 1;
        data["maxDepth"] = 50;
        data["threadCount"] = 1;
        data["savePath"] = "result.ppm";

        renderRTImage(data);        
        
        mainLoop();
        cleanup();
    }

private:
    VkInstance instance;
    Device *device;
    ImageService *imageService;
    DescriptorService *descriptorService;
    std::vector<ViewPort *> viewPorts;
    std::vector<SwapChain *> swapChains;

    VkDebugUtilsMessengerEXT debugMessenger;

    std::vector<VkSurfaceKHR> surfaces;

    VkRenderPass renderPass;
    VkPipelineLayout pipelineLayout;
    std::vector<VkPipeline> graphicsPipelines;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    std::vector<DepthImage> depthImages;

    std::vector<std::vector<VkCommandBuffer>> commandBuffers;

    std::vector<std::vector<VkSemaphore>> imageAvailableSemaphores;
    std::vector<std::vector<VkSemaphore>> renderFinishedSemaphores;
    std::vector<std::vector<VkFence>> inFlightFences;
    std::vector<std::vector<VkFence>> imagesInFlight;
    std::vector<size_t> currentFrame;

    bool frameInProgress = false;
    bool renderPaused = false;
    bool framebufferResized = false;
    bool modelsUpdated = false;

    void initWindow()
    {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    }

    void renderRTImage(json data)
    {
        const int samples_per_pixel = data["samplesPerPixel"];
        const int max_depth = data["maxDepth"];
        const int thread_count = data["threadCount"];
        const std::string savePath = data["savePath"];

        const int image_width = swapChains[0]->swapChainExtent.width;
        const int image_height = swapChains[0]->swapChainExtent.height;
        const glm::vec3 background = glm::vec3(0);

        glm::vec3 lower_left_corner(-2.0, -1.0, -1.0);
        glm::vec3 horizontal(4.0, 0.0, 0.0);
        glm::vec3 vertical(0.0, 2.0, 0.0);
        glm::vec3 origin(0.0, 0.0, 0.0);

        std::ofstream myfile;
        myfile.open(savePath);
        myfile << "P3\n"
               << image_width << ' ' << image_height << "\n255\n";
        render_thread_data *default_data;

        default_data = (render_thread_data *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                                       sizeof(render_thread_data));

        default_data->world = sceneGraph->world;
        default_data->image_width = image_width;
        default_data->image_height = image_height;
        default_data->samples_per_pixel = samples_per_pixel;
        default_data->max_depth = max_depth;
        default_data->background = background;
        default_data->camera = mainCamera;

        std::vector<render_thread_data *> threads_data;
        threads_data.resize(thread_count);

        for (int t = 0; t < thread_count; t++)
        {
            threads_data[t] = (render_thread_data *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY,
                                                              sizeof(render_thread_data));
            *threads_data[t] = *default_data;
            threads_data[t]->results = std::vector<glm::vec3>();
            threads_data[t]->thread_id = t;

            hThreadArray[t] = CreateThread(
                NULL,                 // default security attributes
                0,                    // use default stack size
                renderThread,         // thread function name
                threads_data[t],      // argument to thread function
                0,                    // use default creation flags
                &dwThreadIdArray[t]); // returns the thread identifier

            if (dwThreadIdArray[t] == NULL)
            {
                throw std::runtime_error("failed to launch main thread!");
            }
        }

        WaitForMultipleObjects(thread_count, hThreadArray, TRUE, INFINITE);

        for (int i = 0; i < threads_data[0]->results.size(); i++)
        {
            glm::vec3 color(0, 0, 0);
            int samples_per_pixels_counter = 0;

            for (int j = 0; j < thread_count; j++)
            {
                color += threads_data[j]->results[i];
                samples_per_pixels_counter += threads_data[j]->samples_per_pixel;
            }

            Utils::write_color(color, myfile, samples_per_pixels_counter);
        }

        myfile.close();
    }

    void applyDefaultTexture(std::shared_ptr<Node> node)
    {
        node->material = make_shared<Lambertian>(glm::vec3(1), imageService->createTexture(PURPLE_CHECKERBOARD_TEXTURE_PATH));
    }
    
    HittableList final_scene() {
        shared_ptr<Texture> texture = imageService->createTexture(WHITE_TEXTURE_PATH);
        HittableList boxes1;
        auto ground = make_shared<Lambertian>(glm::vec3({ 0.48, 0.83, 0.53 }), texture);

        const int boxes_per_side = 20;
        for (int i = 0; i < boxes_per_side; i++) {
            for (int j = 0; j < boxes_per_side; j++) {
                auto w = 100.0;
                auto x0 = -1000.0 + i * w;
                auto z0 = -1000.0 + j * w;
                auto y0 = 0.0;
                auto x1 = x0 + w;
                auto y1 = Utils::random_double(1, 101);
                auto z1 = z0 + w;

                boxes1.add(make_shared<box>(glm::vec3(x0, y0, z0), glm::vec3(x1, y1, z1), ground));
            }
        }

        HittableList objects;

        objects.add(make_shared<BvhNode>(boxes1));

        auto light = make_shared<DiffuseLight>(glm::vec3(7), texture);
        objects.add(make_shared<xz_rect>(123, 423, 147, 412, 554, light));

        auto moving_sphere_material = make_shared<Lambertian>(glm::vec3(0.7, 0.3, 0.1), texture);
        objects.add(make_shared<Sphere>(glm::vec3(400, 400, 200), 50, moving_sphere_material));

        objects.add(make_shared<Sphere>(glm::vec3(260, 150, 45), 50, make_shared<Dielectric>(glm::vec3(1), 1.5, texture)));
        objects.add(make_shared<Sphere>(glm::vec3(0, 150, 145), 50, make_shared<Metal>(glm::vec3(0.8, 0.8, 0.9), 10.0, texture)));

        auto boundary = make_shared<Sphere>(glm::vec3(360, 150, 145), 70, make_shared<Dielectric>(glm::vec3(1), 1.5, texture));
        objects.add(boundary);
        boundary = make_shared<Sphere>(glm::vec3(0, 0, 0), 5000, make_shared<Dielectric>(glm::vec3(1), 1.5, texture));

        auto emat = make_shared<Lambertian>(glm::vec3(1), imageService->createTexture("resources/textures/paolomu.png"));
        objects.add(make_shared<Sphere>(glm::vec3(400, 200, 400), 100, emat));

        HittableList boxes2;
        auto white = make_shared<Lambertian>(glm::vec3(.73, .73, .73), texture);
        int ns = 1000;

        return objects;
    }

    void initTestScene()
    {
        shared_ptr<Texture> texture = imageService->createTexture(WHITE_TEXTURE_PATH);
        shared_ptr<Texture> textureChecker = imageService->createTexture(PURPLE_CHECKERBOARD_TEXTURE_PATH);
        shared_ptr<Texture> textureSkybox = imageService->createTexture("resources/textures/milkyway_skybox.png");
        //shared_ptr<Texture> texturePaolomu = imageService->createTexture("resources/textures/paolomu.png");
        //shared_ptr<Texture> textureTotoro = imageService->createTexture("resources/textures/mesh.jpeg");
        //shared_ptr<Texture> normalTotoro = imageService->createTexture("resources/textures/meshnm.jpeg");

        //std::shared_ptr<Node> paolocube = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //paolocube->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, paolocube->model);
        //paolocube->name = "paolocube";
        ////paolocube->transformation.setTranslation(glm::vec3({ -0.2, -0.2, 1 }));
        //paolocube->transformation.setScale(glm::vec3({ 1, 1, 0.1 }));
        ////paolocube->transformation.setRotation(glm::vec3({ 0, 0.2, 0 }));
        ////paolocube->material = make_shared<Lambertian>(glm::vec3(1), textureChecker);
        //paolocube->material = make_shared<DiffuseLight>(glm::vec3(1), textureChecker);

        //std::shared_ptr<Node> red = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //red->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, red->model);
        //red->name = "red";
        //red->transformation.setTranslation(glm::vec3({ 1, 0, 1 }));
        //red->material = make_shared<Lambertian>(glm::vec3({ 1, 0, 0 }), texture);

        std::shared_ptr<Node> skybox = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        skybox->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        ModelLoader::loadModelFromFile("resources/models/skybox.obj", ModelFileType::OBJ, skybox->model);
        skybox->name = "skybox";
        skybox->transformation.setScale(glm::vec3(10000));
        skybox->material = make_shared<DiffuseLight>(glm::vec3(0.4), textureSkybox);

        std::shared_ptr<Node> disk = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        disk->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        ModelLoader::loadModelFromFile("resources/models/disk.obj", ModelFileType::OBJ, disk->model);
        disk->name = "disk";
        disk->transformation.setTranslation(glm::vec3({ 0, -10, 0.1 }));
        disk->transformation.setScale(glm::vec3(0.4));
        disk->material = make_shared<DiffuseLight>(glm::vec3(1), texture);

        //std::shared_ptr<Node> green = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //green->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, green->model);
        //green->name = "green";
        //green->transformation.setTranslation(glm::vec3({ -1, 0, 1 }));
        //green->material = make_shared<Lambertian>(glm::vec3({ 0, 1, 0 }), texture);

        //std::shared_ptr<Node> floor = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //floor->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, floor->model);
        //floor->name = "floor";
        //floor->transformation.setTranslation(glm::vec3({ 0, -1, 1 }));
        ////floor->material = make_shared<Lambertian>(glm::vec3({ 0.9, 0.9, 0.9 }), texture);
        //floor->material = make_shared<DiffuseLight>(glm::vec3(1), texture);

        /*std::shared_ptr<Node> back = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        back->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        ModelFactory::getShape(GeometryType::CUBE, back->model);
        back->name = "back";
        back->transformation.setScale(glm::vec3(10000, 10000, 0.1));
        back->transformation.setTranslation(glm::vec3(0, 0, 10000));
        back->material = make_shared<DiffuseLight>(glm::vec3(0.4), textureChecker);*/

        //std::shared_ptr<Node> top = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //top->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, top->model);
        //top->name = "top";
        //top->transformation.setTranslation(glm::vec3({ 0, 1, 1 }));
        //top->material = make_shared<Metal>(glm::vec3(1), 0.1, texture);

        /*std::shared_ptr<Node> sphere = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        sphere->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        ModelFactory::getShape(GeometryType::SPHERE, sphere->model);
        sphere->name = "sphere";
        sphere->transformation.setTranslation(glm::vec3({ 0, 0, 2 }));
        sphere->transformation.setScale(glm::vec3({ 0.001, 1, 1 }));
        sphere->material = make_shared<DiffuseLight>(glm::vec3(1), texture);*/

        //std::shared_ptr<Node> light = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //light->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, light->model);
        //light->name = "light";
        //light->transformation.setTranslation(glm::vec3({ 0, 0.5, 1 }));
        //light->transformation.setScale(glm::vec3({ 0.4, 0.1, 0.4 }));
        //light->material = make_shared<DiffuseLight>(glm::vec3(1), texture);


        //std::shared_ptr<Node> backLight = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //backLight->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelFactory::getShape(GeometryType::CUBE, backLight->model);
        //backLight->name = "backLight";
        //backLight->transformation.setTranslation(glm::vec3({ 0, 0, 6 }));
        //backLight->transformation.setScale(glm::vec3({ 10, 10, 0.4 }));
        //backLight->material = make_shared<DiffuseLight>(glm::vec3(1), texture);


        //std::shared_ptr<Node> sword = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //sword->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelLoader::loadModelFromFile("resources/models/R2_Sword.stl", ModelFileType::STL, sword->model);
        //sword->name = "sword";
        //sword->transformation.setTranslation(glm::vec3({ 0.2, 0, 0.1 }));
        //sword->transformation.setScale(glm::vec3({ 0.045, 0.045, 0.045 }));
        //sword->material = make_shared<Metal>(glm::vec3({0.8, 0.3, 0.3}), 0.4, texture);
        ////sword->material = make_shared<Dielectric>(glm::vec3(1), 1.517, texture);

        //std::shared_ptr<Node> totoro = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //totoro->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelLoader::loadModelFromFile("resources/models/totoro.obj", ModelFileType::OBJ, totoro->model);
        //totoro->name = "totoro";
        ////totoro->transformation.setTranslation(glm::vec3({ 0.2, 0, 0.1 }));
        //totoro->transformation.setScale(glm::vec3(5));
        //totoro->transformation.setRotation(glm::vec3({3.1416, 0, 0}));
        //totoro->material = make_shared<Lambertian>(glm::vec3(1), textureTotoro);
        //totoro->material->normalMap = normalTotoro;
        ////totoro->material = make_shared<Metal>(glm::vec3({ 0.8, 0.3, 0.3 }), 0.4, texture);


        //std::shared_ptr<Node> yoshi = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        //yoshi->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        //ModelLoader::loadModelFromFile("resources/models/yoshi.fbx", ModelFileType::FBX, yoshi->model);
        //yoshi->name = "yoshi";
        //yoshi->transformation.setTranslation(glm::vec3({ 0, -0.55, 0 }));
        //yoshi->transformation.setScale(glm::vec3(0.6));
        //yoshi->material = make_shared<Metal>(glm::vec3(1), 0, texture);

        //sceneGraph->addNode(sceneGraph->getBase()->id, red);
        //sceneGraph->addNode(sceneGraph->getBase()->id, green);
        //sceneGraph->addNode(sceneGraph->getBase()->id, floor);
        //sceneGraph->addNode(sceneGraph->getBase()->id, back);
        //sceneGraph->addNode(sceneGraph->getBase()->id, top);
        //sceneGraph->addNode(sceneGraph->getBase()->id, light);
        //sceneGraph->addNode(sceneGraph->getBase()->id, backLight);
        //sceneGraph->addNode(sceneGraph->getBase()->id, paolocube);
        //sceneGraph->addNode(sceneGraph->getBase()->id, totoro);
        //sceneGraph->addNode(sceneGraph->getBase()->id, yoshi);
        sceneGraph->addNode(sceneGraph->getBase()->id, skybox);
        //sceneGraph->addNode(sceneGraph->getBase()->id, disk);
        //sceneGraph->addNode(sceneGraph->getBase()->id, sword);
    }

    void initGeometry()
    {
        std::shared_ptr<Node> base = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        base->name = "base";
        base->transformation.setScale(glm::vec3(100));

        sceneGraph = std::shared_ptr<SceneGraph>(new SceneGraph(device, base));

        initTestScene();

        /*std::shared_ptr<Node> cube = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
        cube->model = new Model(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        ModelFactory::getShape(GeometryType::CUBE, cube->model);
        cube->name = "Just a cube";
        applyDefaultTexture(cube);

        sceneGraph->addNode(sceneGraph->getBase()->id, cube);*/


        sceneGraph->generateModelRenderInfos(device, viewPorts.size(), swapChains[0]->swapChainImages.size());
        sceneGraph->generateBuffers(device);
    }

    void initCamera()
    {
        const int image_width = swapChains[0]->swapChainExtent.width;
        const int image_height = swapChains[0]->swapChainExtent.height;
        const double aspect_ratio = (double)image_width / (double)image_height;



        glm::vec3 lookfrom(0, 0, 100);
        glm::vec3 lookat(0, 0, 0);
        glm::vec3 vup(0, 1, 0);
        float vfov = 60.0;

        mainCamera = new Camera(lookfrom, lookat, vup, vfov, aspect_ratio);
        mainCamera->name = "main camera";
        mainCamera->id = UuidFactory::getUid();
        mainCamera->projectionType = CameraProjectionType::PERSPECTIVE;

        ready = true;
    }

    void initVulkan()
    {
        createInstance();
        setupDebugMessenger();

        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        for (int i = 0; i < WINDOW_COUNT; i++)
        {
            viewPorts.push_back(new ViewPort(instance, WIDTH, HEIGHT));
        }

        device = new Device(instance, viewPorts[0]->surface, enableValidationLayers);
        imageService = new ImageService(device);

        for (int i = 0; i < viewPorts.size(); i++)
        {
            swapChains.push_back(new SwapChain(device, viewPorts[i], imageService));
        }

        initGeometry();
        initCamera();
        createRenderPass();

        descriptorService = new DescriptorService(device, swapChains[0]->swapChainImages.size());
        descriptorService->createDescriptorSets(sceneGraph->modelRenderInfos, viewPorts.size());

        createGraphicsPipeline();

        for (int i = 0; i < viewPorts.size(); i++)
        {
            depthImages.push_back(imageService->createDepthResource(swapChains[i]));
        }

        createFramebuffers();
        createCommandBuffers();
        createSyncObjects();
    }

    void mainLoop()
    {
        bool shouldClose = false;

        while (!shouldClose)
        {
            frameInProgress = false;

            while (renderPaused)
            {
                Sleep(150);
            }
            glfwPollEvents();
            frameInProgress = true;
            drawFrame();

            for (size_t i = 0; i < viewPorts.size(); i++)
            {
                if (glfwWindowShouldClose(viewPorts[i]->glfwWindow))
                {
                    shouldClose = true;
                }
            }
        }

        vkDeviceWaitIdle(device->vkDevice);
    }

    void cleanupSwapChain()
    {
        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            imageService->cleanupDepthImage(depthImages[i]);
        }
        depthImages = std::vector<DepthImage>();

        for (size_t i = 0; i < swapChains.size(); i++)
        {
            delete swapChains[i];
        }
        swapChains = std::vector<SwapChain *>();

        for (size_t i = 0; i < commandBuffers.size(); i++)
        {
            vkFreeCommandBuffers(device->vkDevice, device->commandPool, static_cast<uint32_t>(commandBuffers[i].size()), commandBuffers[i].data());
        }

        for (size_t i = 0; i < graphicsPipelines.size(); i++)
        {
            vkDestroyPipeline(device->vkDevice, graphicsPipelines[i], nullptr);
        }
        graphicsPipelines = std::vector<VkPipeline>();

        for (size_t i = 0; i < sceneGraph->modelRenderInfos.size(); i++)
        {
            shared_ptr<ModelRenderInfo> info = sceneGraph->modelRenderInfos[i];
            for (size_t j = 0; j < info->descriptorPools.size(); j++)
            {
                vkDestroyDescriptorPool(device->vkDevice, info->descriptorPools[j], nullptr);
            }
        }
    }

    void cleanup()
    {
        cleanupSwapChain();
        sceneGraph->cleanupBuffers();

        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            for (size_t j = 0; j < MAX_FRAMES_IN_FLIGHT; j++)
            {
                vkDestroySemaphore(device->vkDevice, renderFinishedSemaphores[i][j], nullptr);
                vkDestroySemaphore(device->vkDevice, imageAvailableSemaphores[i][j], nullptr);
                vkDestroyFence(device->vkDevice, inFlightFences[i][j], nullptr);
            }
        }

        vkDestroyCommandPool(device->vkDevice, device->commandPool, nullptr);

        device->destroyDevice();

        if (enableValidationLayers)
        {
            DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
        }

        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            vkDestroySurfaceKHR(instance, surfaces[i], nullptr);
        }

        vkDestroyInstance(instance, nullptr);

        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            delete viewPorts[i];
        }
        viewPorts = std::vector<ViewPort *>();

        glfwTerminate();
    }

    void recreateSwapChain()
    {
        int width = 0, height = 0;
        while (width == 0 || height == 0)
        {
            glfwGetFramebufferSize(viewPorts[0]->glfwWindow, &width, &height);
            glfwWaitEvents();
        }

        vkDeviceWaitIdle(device->vkDevice);

        cleanupSwapChain();

        for (int i = 0; i < viewPorts.size(); i++)
        {
            swapChains.push_back(new SwapChain(device, viewPorts[i], imageService));
        }

        createRenderPass();
        createGraphicsPipeline();

        for (int i = 0; i < viewPorts.size(); i++)
        {
            depthImages.push_back(imageService->createDepthResource(swapChains[i]));
        }

        createFramebuffers();

        sceneGraph->generateModelRenderInfos(device, viewPorts.size(), swapChains[0]->swapChainImages.size());

        sceneGraph->generateBuffers(device);
        descriptorService->createDescriptorSets(sceneGraph->modelRenderInfos, viewPorts.size());

        createCommandBuffers();
    }

    void createInstance()
    {
        if (enableValidationLayers && !checkValidationLayerSupport())
        {
            throw std::runtime_error("validation layers requested, but not available!");
        }
        VkApplicationInfo appInfo = {};
        appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        appInfo.pApplicationName = "vkMassive";
        appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.pEngineName = "No Engine";
        appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        appInfo.apiVersion = VK_API_VERSION_1_0;

        VkInstanceCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        createInfo.pApplicationInfo = &appInfo;

        auto extensions = getRequiredExtensions();
        createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        createInfo.ppEnabledExtensionNames = extensions.data();

        VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
        if (enableValidationLayers)
        {
            createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
            createInfo.ppEnabledLayerNames = validationLayers.data();

            populateDebugMessengerCreateInfo(debugCreateInfo);
            createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *)&debugCreateInfo;
        }
        else
        {
            createInfo.enabledLayerCount = 0;

            createInfo.pNext = nullptr;
        }

        if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to create instance!");
        }
    }

    void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT &createInfo)
    {
        createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        createInfo.pfnUserCallback = debugCallback;
    }

    void setupDebugMessenger()
    {
        if (!enableValidationLayers)
            return;

        VkDebugUtilsMessengerCreateInfoEXT createInfo;
        populateDebugMessengerCreateInfo(createInfo);

        if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to set up debug messenger!");
        }
    }

    void createRenderPass()
    {
        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            VkAttachmentDescription colorAttachment = {};
            colorAttachment.format = swapChains[i]->swapChainImageFormat;
            colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
            colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
            colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

            VkAttachmentDescription depthAttachment = {};
            depthAttachment.format = imageService->findDepthFormat();
            depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
            depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
            depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
            depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
            depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkAttachmentReference colorAttachmentRef = {};
            colorAttachmentRef.attachment = 0;
            colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

            VkAttachmentReference depthAttachmentRef = {};
            depthAttachmentRef.attachment = 1;
            depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

            VkSubpassDescription subpass = {};
            subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
            subpass.colorAttachmentCount = 1;
            subpass.pColorAttachments = &colorAttachmentRef;
            subpass.pDepthStencilAttachment = &depthAttachmentRef;

            VkSubpassDependency dependency = {};
            dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
            dependency.dstSubpass = 0;
            dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dependency.srcAccessMask = 0;
            dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

            std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
            VkRenderPassCreateInfo renderPassInfo = {};
            renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
            renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
            renderPassInfo.pAttachments = attachments.data();
            renderPassInfo.subpassCount = 1;
            renderPassInfo.pSubpasses = &subpass;
            renderPassInfo.dependencyCount = 1;
            renderPassInfo.pDependencies = &dependency;

            if (vkCreateRenderPass(device->vkDevice, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
            {
                throw std::runtime_error("failed to create render pass!");
            }
        }
    }

    void createGraphicsPipeline()
    {
        auto vertShaderCode = readFile("resources/shaders/vert.spv");
        auto fragShaderCode = readFile("resources/shaders/frag.spv");

        VkShaderModule vertShaderModule = createShaderModule(vertShaderCode);
        VkShaderModule fragShaderModule = createShaderModule(fragShaderCode);

        VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
        vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
        vertShaderStageInfo.module = vertShaderModule;
        vertShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
        fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        fragShaderStageInfo.module = fragShaderModule;
        fragShaderStageInfo.pName = "main";

        VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

        VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
        vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

        auto bindingDescription = Vertex::getBindingDescription();
        auto attributeDescriptions = Vertex::getAttributeDescriptions();

        vertexInputInfo.vertexBindingDescriptionCount = 1;
        vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
        vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
        vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

        VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
        inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        inputAssembly.primitiveRestartEnable = VK_FALSE;

        graphicsPipelines.resize(WINDOW_COUNT);
        for (size_t i = 0; i < WINDOW_COUNT; i++)
        {
            VkViewport viewport = {};
            viewport.x = 0.0f;
            viewport.y = 0.0f;
            viewport.width = (float)swapChains[i]->swapChainExtent.width;
            viewport.height = (float)swapChains[i]->swapChainExtent.height;
            viewport.minDepth = 0.0f;
            viewport.maxDepth = 1.0f;

            VkRect2D scissor = {};
            scissor.offset = {0, 0};
            scissor.extent = swapChains[i]->swapChainExtent;

            VkPipelineViewportStateCreateInfo viewportState = {};
            viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
            viewportState.viewportCount = 1;
            viewportState.pViewports = &viewport;
            viewportState.scissorCount = 1;
            viewportState.pScissors = &scissor;

            VkPipelineRasterizationStateCreateInfo rasterizer = {};
            rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
            rasterizer.depthClampEnable = VK_FALSE;
            rasterizer.rasterizerDiscardEnable = VK_FALSE;
            rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
            rasterizer.lineWidth = 1.0f;
            rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
            rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
            rasterizer.depthBiasEnable = VK_FALSE;

            VkPipelineMultisampleStateCreateInfo multisampling = {};
            multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
            multisampling.sampleShadingEnable = VK_FALSE;
            multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

            VkPipelineDepthStencilStateCreateInfo depthStencil = {};
            depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
            depthStencil.depthTestEnable = VK_TRUE;
            depthStencil.depthWriteEnable = VK_TRUE;
            depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
            depthStencil.depthBoundsTestEnable = VK_FALSE;
            depthStencil.minDepthBounds = 0.0f; // Optional
            depthStencil.maxDepthBounds = 1.0f; // Optional
            depthStencil.stencilTestEnable = VK_FALSE;
            depthStencil.front = {}; // Optional
            depthStencil.back = {};  // Optional

            VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
            colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
            colorBlendAttachment.blendEnable = VK_FALSE;

            VkPipelineColorBlendStateCreateInfo colorBlending = {};
            colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
            colorBlending.logicOpEnable = VK_FALSE;
            colorBlending.logicOp = VK_LOGIC_OP_COPY;
            colorBlending.attachmentCount = 1;
            colorBlending.pAttachments = &colorBlendAttachment;
            colorBlending.blendConstants[0] = 0.0f;
            colorBlending.blendConstants[1] = 0.0f;
            colorBlending.blendConstants[2] = 0.0f;
            colorBlending.blendConstants[3] = 0.0f;

            VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
            pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
            pipelineLayoutInfo.setLayoutCount = 1;
            pipelineLayoutInfo.pSetLayouts = &descriptorService->descriptorSetLayout;

            if (vkCreatePipelineLayout(device->vkDevice, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
            {
                throw std::runtime_error("failed to create pipeline layout!");
            }

            VkGraphicsPipelineCreateInfo pipelineInfo = {};
            pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
            pipelineInfo.stageCount = 2;
            pipelineInfo.pStages = shaderStages;
            pipelineInfo.pVertexInputState = &vertexInputInfo;
            pipelineInfo.pInputAssemblyState = &inputAssembly;
            pipelineInfo.pViewportState = &viewportState;
            pipelineInfo.pRasterizationState = &rasterizer;
            pipelineInfo.pMultisampleState = &multisampling;
            pipelineInfo.pDepthStencilState = &depthStencil;
            pipelineInfo.pColorBlendState = &colorBlending;
            pipelineInfo.layout = pipelineLayout;
            pipelineInfo.renderPass = renderPass;
            pipelineInfo.subpass = 0;
            pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

            if (vkCreateGraphicsPipelines(device->vkDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipelines[i]) != VK_SUCCESS)
            {
                throw std::runtime_error("failed to create graphics pipeline!");
            }
        }

        vkDestroyShaderModule(device->vkDevice, fragShaderModule, nullptr);
        vkDestroyShaderModule(device->vkDevice, vertShaderModule, nullptr);
    }

    void createFramebuffers()
    {
        currentFrame.resize(viewPorts.size());
        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            currentFrame[i] = (size_t)0;
            swapChains[i]->swapChainFramebuffers.resize(swapChains[i]->swapChainImageView.size());

            for (size_t j = 0; j < swapChains[i]->swapChainImageView.size(); j++)
            {
                std::array<VkImageView, 2> attachments = {
                    swapChains[i]->swapChainImageView[j],
                    depthImages[i].depthImageView};

                VkFramebufferCreateInfo framebufferInfo = {};
                framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                framebufferInfo.renderPass = renderPass;
                framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
                framebufferInfo.pAttachments = attachments.data();
                framebufferInfo.width = swapChains[i]->swapChainExtent.width;
                framebufferInfo.height = swapChains[i]->swapChainExtent.height;
                framebufferInfo.layers = 1;

                if (vkCreateFramebuffer(device->vkDevice, &framebufferInfo, nullptr, &swapChains[i]->swapChainFramebuffers[j]) != VK_SUCCESS)
                {
                    throw std::runtime_error("failed to create framebuffer!");
                }
            }
        }
    }

    void createCommandBuffers()
    {
        commandBuffers.resize(viewPorts.size());
        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            commandBuffers[i].resize(swapChains[i]->swapChainFramebuffers.size());

            VkCommandBufferAllocateInfo allocInfo = {};
            allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
            allocInfo.commandPool = device->commandPool;
            allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
            allocInfo.commandBufferCount = (uint32_t)commandBuffers[i].size();

            if (vkAllocateCommandBuffers(device->vkDevice, &allocInfo, commandBuffers[i].data()) != VK_SUCCESS)
            {
                throw std::runtime_error("failed to allocate command buffers!");
            }

            for (size_t j = 0; j < commandBuffers[i].size(); j++)
            {
                VkCommandBufferBeginInfo beginInfo = {};
                beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

                if (vkBeginCommandBuffer(commandBuffers[i][j], &beginInfo) != VK_SUCCESS)
                {
                    throw std::runtime_error("failed to begin recording command buffer!");
                }

                VkRenderPassBeginInfo renderPassInfo = {};
                renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
                renderPassInfo.renderPass = renderPass;
                renderPassInfo.framebuffer = swapChains[i]->swapChainFramebuffers[j];
                renderPassInfo.renderArea.offset = {0, 0};
                renderPassInfo.renderArea.extent = swapChains[i]->swapChainExtent;

                std::array<VkClearValue, 2> clearValues = {};
                clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
                clearValues[1].depthStencil = {1.0f, 0};

                renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
                renderPassInfo.pClearValues = clearValues.data();

                vkCmdBeginRenderPass(commandBuffers[i][j], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

                vkCmdBindPipeline(commandBuffers[i][j], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipelines[i]);

                VkDeviceSize offsets[] = {0};
                vkCmdBindVertexBuffers(commandBuffers[i][j], 0, 1, &sceneGraph->vertexBuffer->vertexBuffer, offsets);
                vkCmdBindIndexBuffer(commandBuffers[i][j], sceneGraph->indexBuffer->indexBuffer, 0, VK_INDEX_TYPE_UINT32);

                int vertexCount = 0;
                for (size_t k = 0; k < sceneGraph->modelRenderInfos.size(); k++)
                {
                    shared_ptr<ModelRenderInfo> info = sceneGraph->modelRenderInfos[k];

                    VkDescriptorSet finalSet = info->descriptorSets[i][j];
                    vkCmdBindDescriptorSets(commandBuffers[i][j], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &finalSet, 0, nullptr);
                    vkCmdDraw(commandBuffers[i][j], info->nodeRef->model->vertices.size(), 1, info->nodeRef->model->vertexOffset, 0);
                }

                vkCmdEndRenderPass(commandBuffers[i][j]);

                if (vkEndCommandBuffer(commandBuffers[i][j]) != VK_SUCCESS)
                {
                    throw std::runtime_error("failed to record command buffer!");
                }
            }
        }
    }

    void createSyncObjects()
    {
        imageAvailableSemaphores.resize(viewPorts.size());
        renderFinishedSemaphores.resize(viewPorts.size());
        inFlightFences.resize(viewPorts.size());
        imagesInFlight.resize(viewPorts.size());

        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            imageAvailableSemaphores[i].resize(MAX_FRAMES_IN_FLIGHT);
            renderFinishedSemaphores[i].resize(MAX_FRAMES_IN_FLIGHT);
            inFlightFences[i].resize(MAX_FRAMES_IN_FLIGHT);
            imagesInFlight[i].resize(swapChains[i]->swapChainImages.size(), VK_NULL_HANDLE);

            VkSemaphoreCreateInfo semaphoreInfo = {};
            semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

            VkFenceCreateInfo fenceInfo = {};
            fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
            fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

            for (size_t j = 0; j < MAX_FRAMES_IN_FLIGHT; j++)
            {
                if (vkCreateSemaphore(device->vkDevice, &semaphoreInfo, nullptr, &imageAvailableSemaphores[i][j]) != VK_SUCCESS ||
                    vkCreateSemaphore(device->vkDevice, &semaphoreInfo, nullptr, &renderFinishedSemaphores[i][j]) != VK_SUCCESS ||
                    vkCreateFence(device->vkDevice, &fenceInfo, nullptr, &inFlightFences[i][j]) != VK_SUCCESS)
                {
                    throw std::runtime_error("failed to create synchronization objects for a frame!");
                }
            }
        }
    }

    void updateUniformBuffers(uint32_t currentImage, size_t currentWindow)
    {
        for (size_t i = 0; i < sceneGraph->modelRenderInfos.size(); i++)
        {
            shared_ptr<ModelRenderInfo> info = sceneGraph->modelRenderInfos[i];
            float time = 0.0f;

            UniformBufferObject ubo = {};
            ubo.model = info->calculatedTransformMatrix;
            ubo.view = mainCamera->view;

            if (mainCamera->projectionType == CameraProjectionType::PERSPECTIVE)
            {
                ubo.proj = glm::perspective(glm::radians(mainCamera->vfov), (float)(swapChains[currentWindow]->swapChainExtent.width / (float)swapChains[currentWindow]->swapChainExtent.height), 0.1f, 1000.0f);
                ubo.proj[1][1] *= -1;
            }
            else
            {
                ubo.proj = glm::ortho(-(swapChains[currentWindow]->swapChainExtent.width / 2.0f), (swapChains[currentWindow]->swapChainExtent.width / 2.0f), (swapChains[currentWindow]->swapChainExtent.height / 2.0f), -(swapChains[currentWindow]->swapChainExtent.height / 2.0f), -1000.0f, 1000.0f);
            }

            void *data;

            vkMapMemory(device->vkDevice, info->uniformBuffers[currentWindow][currentImage]->uniformBufferMemory, 0, sizeof(ubo), 0, &data);
            memcpy(data, &ubo, sizeof(ubo));
            vkUnmapMemory(device->vkDevice, info->uniformBuffers[currentWindow][currentImage]->uniformBufferMemory);
        }
    }

    void drawFrame()
    {
        uint32_t imageIndex;
        for (size_t i = 0; i < viewPorts.size(); i++)
        {
            vkWaitForFences(device->vkDevice, 1, &inFlightFences[i][currentFrame[i]], VK_TRUE, UINT64_MAX);
            VkResult result = vkAcquireNextImageKHR(device->vkDevice, swapChains[i]->swapChain, UINT64_MAX, imageAvailableSemaphores[i][currentFrame[i]], VK_NULL_HANDLE, &imageIndex);

            if (result == VK_ERROR_OUT_OF_DATE_KHR)
            {
                recreateSwapChain();
                return;
            }
            else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
            {
                throw std::runtime_error("failed to acquire swap chain image!");
            }

            updateUniformBuffers(imageIndex, i);

            if (imagesInFlight[i][imageIndex] != VK_NULL_HANDLE)
            {
                vkWaitForFences(device->vkDevice, 1, &imagesInFlight[i][imageIndex], VK_TRUE, UINT64_MAX);
            }
            imagesInFlight[i][imageIndex] = inFlightFences[i][currentFrame[i]];

            VkSubmitInfo submitInfo = {};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

            VkSemaphore waitSemaphores[] = {imageAvailableSemaphores[i][currentFrame[i]]};
            VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = waitSemaphores;
            submitInfo.pWaitDstStageMask = waitStages;

            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffers[i][imageIndex];

            VkSemaphore signalSemaphores[] = {renderFinishedSemaphores[i][currentFrame[i]]};
            submitInfo.signalSemaphoreCount = 1;
            submitInfo.pSignalSemaphores = signalSemaphores;

            vkResetFences(device->vkDevice, 1, &inFlightFences[i][currentFrame[i]]);

            if (vkQueueSubmit(device->graphicsQueue, 1, &submitInfo, inFlightFences[i][currentFrame[i]]) != VK_SUCCESS)
            {
                throw std::runtime_error("failed to submit draw command buffer!");
            }

            VkPresentInfoKHR presentInfo = {};
            presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

            presentInfo.waitSemaphoreCount = 1;
            presentInfo.pWaitSemaphores = signalSemaphores;

            VkSwapchainKHR inputSwapChains[] = {swapChains[i]->swapChain};

            presentInfo.swapchainCount = 1;
            presentInfo.pSwapchains = inputSwapChains;

            presentInfo.pImageIndices = &imageIndex;

            result = vkQueuePresentKHR(device->presentQueue, &presentInfo);

            if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || framebufferResized)
            {
                framebufferResized = false;
                recreateSwapChain();
            }
            else if (modelsUpdated)
            {
                modelsUpdated = false;
                //recreateBuffers();
            }
            else if (result != VK_SUCCESS)
            {
                throw std::runtime_error("failed to present swap chain image!");
            }

            currentFrame[i] = (currentFrame[i] + 1) % MAX_FRAMES_IN_FLIGHT;
        }
    }

    VkShaderModule createShaderModule(const std::vector<char> &code)
    {
        VkShaderModuleCreateInfo createInfo = {};
        createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        createInfo.codeSize = code.size();
        createInfo.pCode = reinterpret_cast<const uint32_t *>(code.data());

        VkShaderModule shaderModule;
        if (vkCreateShaderModule(device->vkDevice, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to create shader module!");
        }

        return shaderModule;
    }

    std::vector<const char *> getRequiredExtensions()
    {
        uint32_t glfwExtensionCount = 0;
        const char **glfwExtensions;
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        std::vector<const char *> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

        if (enableValidationLayers)
        {
            extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        return extensions;
    }

    bool checkValidationLayerSupport()
    {
        uint32_t layerCount;
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        for (const char *layerName : validationLayers)
        {
            bool layerFound = false;

            for (const auto &layerProperties : availableLayers)
            {
                if (strcmp(layerName, layerProperties.layerName) == 0)
                {
                    layerFound = true;
                    break;
                }
            }

            if (!layerFound)
            {
                return false;
            }
        }

        return true;
    }

    static std::vector<char> readFile(const std::string &filename)
    {
        std::ifstream file(filename, std::ios::ate | std::ios::binary);

        if (!file.is_open())
        {
            throw std::runtime_error("failed to open file!");
        }

        size_t fileSize = (size_t)file.tellg();
        std::vector<char> buffer(fileSize);

        file.seekg(0);
        file.read(buffer.data(), fileSize);

        file.close();

        return buffer;
    }

    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *pUserData)
    {
        std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

        return VK_FALSE;
    }
};

#ifdef NODE_API
VkMassive app;

DWORD WINAPI StartVkApp(LPVOID lpParam)
{
    // Create a mutex with no initial owner

    ghMutex = CreateMutex( 
        NULL,              // default security attributes
        FALSE,             // initially not owned
        NULL);             // unnamed mutex

    if (ghMutex == NULL) 
    {
        printf("CreateMutex error: %d\n", GetLastError());
        return 1;
    }

    app.run();
    
    CloseHandle(ghMutex);

    return 0;
}

NAN_METHOD(hello)
{
    info.GetReturnValue().Set(Nan::New("success").ToLocalChecked());
}

NAN_METHOD(createNode)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);

    size_t nodeId = std::stoi(cppStr);

    std::shared_ptr<Node> newNode = std::shared_ptr<Node>(new Node(UuidFactory::getUid()));
    newNode->name = "unnamed";
    app.sceneGraph->addNode(nodeId, newNode);

    info.GetReturnValue().Set(Nan::New(newNode->jsonEncode().dump()).ToLocalChecked());
}

NAN_METHOD(getNodes)
{
    json data = app.sceneGraph->getBase()->jsonEncode(true);

    info.GetReturnValue().Set(Nan::New(data.dump()).ToLocalChecked());
}

NAN_METHOD(getNode)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);

    size_t nodeId = std::stoi(cppStr);
    json data = app.sceneGraph->nodes[nodeId]->jsonEncode();

    info.GetReturnValue().Set(Nan::New(data.dump()).ToLocalChecked());
}

NAN_METHOD(updateNode)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);

    json data = json::parse(cppStr);
    app.updateNode(data);
}

NAN_METHOD(unlinkNode)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);

    json data = json::parse(cppStr);
    app.unlinkNode(data);
}

NAN_METHOD(linkNode)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);

    json data = json::parse(cppStr);
    app.linkNode(data);
}

NAN_METHOD(getCamera)
{
    info.GetReturnValue().Set(Nan::New(mainCamera->jsonEncode().dump()).ToLocalChecked());
}

NAN_METHOD(startRender)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);
    std::cout << "node call" << std::endl;

    json data = json::parse(cppStr);
    std::cout << "node call" << std::endl;
    app.startRender(data);
}

NAN_METHOD(updateCamera)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);
    json data = json::parse(cppStr);

    mainCamera->updateFromJson(data);

    info.GetReturnValue().Set(Nan::New(mainCamera->jsonEncode().dump()).ToLocalChecked());
}

NAN_METHOD(getNodeMaterial)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);
    size_t nodeId = std::stoi(cppStr);
    json data = app.sceneGraph->nodes[nodeId]->material->jsonEncode();

    info.GetReturnValue().Set(Nan::New(data.dump()).ToLocalChecked());
}

NAN_METHOD(updateNodeMaterial)
{
    v8::Isolate *isolate = info.GetIsolate();
    v8::String::Utf8Value str(isolate, info[0]);
    std::string cppStr(*str);
    json data = json::parse(cppStr);

    app.updateNodeMaterial(data);
}

NAN_METHOD(isReady)
{
    while (!ready)
    {
        Sleep(200);
    }
}

NAN_MODULE_INIT(Init)
{
    hThreadArray[41] = CreateThread(
        NULL,                  // default security attributes
        0,                     // use default stack size
        StartVkApp,            // thread function name
        NULL,                  // argument to thread function
        0,                     // use default creation flags
        &dwThreadIdArray[41]); // returns the thread identifier

    if (dwThreadIdArray[41] == NULL)
    {
        throw std::runtime_error("failed to launch main thread!");
    }

    // Node object access
    Nan::SetMethod(target, "create_node", createNode);
    Nan::SetMethod(target, "get_nodes", getNodes);
    Nan::SetMethod(target, "get_node", getNode);
    Nan::SetMethod(target, "update_node", updateNode);
    Nan::SetMethod(target, "unlink_node", unlinkNode);
    Nan::SetMethod(target, "link_node", linkNode);
    Nan::SetMethod(target, "start_render", startRender);

    Nan::SetMethod(target, "get_camera", getCamera);
    Nan::SetMethod(target, "update_camera", updateCamera);

    Nan::SetMethod(target, "get_node_material", getNodeMaterial);
    Nan::SetMethod(target, "update_node_material", updateNodeMaterial);

    Nan::SetMethod(target, "hello", hello);
    Nan::SetMethod(target, "is_ready", isReady);
}

NAN_MODULE_WORKER_ENABLED(keytar, Init)

NODE_MODULE(hello, Init)
#else
int main()
{
    VkMassive app;

    try
    {
        app.run();
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif