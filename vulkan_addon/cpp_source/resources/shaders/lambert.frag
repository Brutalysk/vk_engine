#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 surface_position;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;

vec3 light_source;

void main() {
    light_source = vec3(1, 0, 0);

    vec3 n = normalize(normal);
    vec3 l = normalize(light_source - surface_position);
    float diffuse_reflexion = max(dot(n, l), 0.0);

    outColor = vec4(fragColor, 1.0);
    outColor = texture(texSampler , fragTexCoord * 1.0);
    outColor = vec4(fragColor * texture(texSampler, fragTexCoord).rgb * diffuse_reflexion, 1.0);
}