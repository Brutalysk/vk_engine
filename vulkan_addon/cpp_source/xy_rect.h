#pragma once
#include "Hittable.h"
class xy_rect : public Hittable
{
public:
    shared_ptr<Material> mp;
    double x0, x1, y0, y1, k;

    xy_rect() {}

    xy_rect(double _x0, double _x1, double _y0, double _y1, double _k, shared_ptr<Material> mat)
        : x0(_x0), x1(_x1), y0(_y0), y1(_y1), k(_k), mp(mat) {};

    virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
    virtual bool bounding_box(aabb& box) const {
        box = aabb(glm::vec3(x0, y0, k - 0.0001), glm::vec3(x1, y1, k + 0.0001));
        return true;
    }
};

