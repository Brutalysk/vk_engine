#pragma once
#include "Hittable.h"
class xz_rect : public Hittable
{
public:
    shared_ptr<Material> mp;
    double x0, x1, z0, z1, k;


    xz_rect() {}

    xz_rect(double _x0, double _x1, double _z0, double _z1, double _k, shared_ptr<Material> mat)
        : x0(_x0), x1(_x1), z0(_z0), z1(_z1), k(_k), mp(mat) {};

    virtual bool hit(const Ray& r, double tmin, double tmax, hit_record& rec) const;
    virtual bool bounding_box(aabb& box) const {
        box = aabb(glm::vec3(x0, k - 0.0001, z0), glm::vec3(x1, k + 0.0001, z1));
        return true;
    }
};

