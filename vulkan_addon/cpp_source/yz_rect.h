#pragma once
#include "Hittable.h"
class yz_rect : public Hittable
{
public:
    shared_ptr<Material> mp;
    double y0, y1, z0, z1, k;

    yz_rect() {}

    yz_rect(double _y0, double _y1, double _z0, double _z1, double _k, shared_ptr<Material> mat)
        : y0(_y0), y1(_y1), z0(_z0), z1(_z1), k(_k), mp(mat) {};

    virtual bool hit(const Ray& r, double t0, double t1, hit_record& rec) const;
    virtual bool bounding_box(aabb& output_box) const {
        output_box = aabb(glm::vec3(k - 0.0001, y0, z0), glm::vec3(k + 0.0001, y1, z1));
        return true;
    }
};

